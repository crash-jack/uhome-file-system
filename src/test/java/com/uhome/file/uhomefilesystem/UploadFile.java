package com.uhome.file.uhomefilesystem;

import cn.hutool.core.io.FileUtil;
import com.uhome.file.uhomefilesystem.config.utils.PdfUtil;
import com.uhome.file.uhomefilesystem.config.utils.UploadImage;
import com.uhome.file.uhomefilesystem.db.dao.FsStoreDetailMapper;
import com.uhome.file.uhomefilesystem.db.dao.FsStoreListMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsStoreDetail;
import com.uhome.file.uhomefilesystem.db.entity.FsStoreList;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreDetailService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreListService;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@SpringBootTest
public class UploadFile {

    private String [] zhengshu = {"营业执照","执照","烟草证","食品证"};

    @Autowired
    private IFsStoreListService fsStoreListService;

    @Autowired
    private IFsStoreDetailService fsStoreDetailService;

    @Test
    public void uploadStoreFile() {
        String path = "F:\\download\\googleDownload\\下载的文件\\errorStore2\\errorStore2";
        List<String> fileNames = getFiles(path);
        List<FsStoreList> storeLists = fsStoreListService.selectList(null,null);
        List<String> errorList = new ArrayList<>();
        List<FsStoreDetail> saveData = new ArrayList<>();
        for (int i = 0; i < fileNames.size(); i++) {
            String [] arrName = fileNames.get(i).split("\\.");
            String [] arr = arrName[0].split("-");
            FsStoreList currentStore = null;
            //校验数据
            if(arr.length==3){
                for (int j = 0; j < storeLists.size(); j++) {
                    if(storeLists.get(j).getName().contains(arr[1])){
                        currentStore = storeLists.get(j);
                    }
                }
                if(currentStore==null){
                    errorList.add(fileNames.get(i));
                    continue;
                }
                boolean arr3Flag = false;
                for (int j = 0; j < zhengshu.length ; j++) {
                    if(arr[2].equals(zhengshu[j] )){
                        arr3Flag = true;
                    }
                }
                if(!arr3Flag){
                    errorList.add(fileNames.get(i));
                    continue;
                }
                ZengshuEnum cid =  ZengshuEnum.getValue(arr[2]);
                if(cid==null){
                    errorList.add(fileNames.get(i));
                    continue;
                }
                FsStoreDetail fsStoreDetail = new FsStoreDetail();
                fsStoreDetail.setName(fileNames.get(i));
                fsStoreDetail.setStoreId(currentStore.getId());
                fsStoreDetail.setCertificateId(Long.parseLong(cid.getKey().toString()));
                fsStoreDetail.setStoreType(currentStore.getStoreType());
                String uuid = UUID.randomUUID().toString();
                fsStoreDetail.setImg("/filesystem/2021/moban/"+ uuid+arrName[1]);
                fsStoreDetail.setImg("/filesystem/2021/moban/"+ uuid+"_1.png");
                saveData.add(fsStoreDetail);
            }else{
                errorList.add(fileNames.get(i));
                continue;
            }
        }
        for (int i = 0; i < errorList.size(); i++) {
            copyFile(path+"\\"+errorList.get(i),"F:\\download\\googleDownload\\下载的文件\\errorStore\\"+errorList.get(i));
        }
        //保存
        int number = 0;
//        for (int i = 0; i < saveData.size(); i++) {
//            int q = fsStoreDetailService.insertSelective(saveData.get(i));
//            number= number+q;
//        }
    }

//    public static void main(String[] args) {
//        String path = "F:\\download\\googleDownload\\下载的文件\\门店营业执照-佛山\\门店营业执照-佛山\\金色领域一店";
//        getFiles(path);
//    }

    public void copyFile(String oldPath, String newPath) {
        try {
            int bytesum = 0;
            int byteread = 0;
            File oldfile = new File(oldPath);
            if (oldfile.exists()) { //文件存在时
                InputStream inStream = new FileInputStream(oldPath); //读入原文件
                FileOutputStream fs = new FileOutputStream(newPath);
                byte[] buffer = new byte[1444];
                int length;
                while ( (byteread = inStream.read(buffer)) != -1) {
                    bytesum += byteread; //字节数 文件大小
                    System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                inStream.close();
            }
        }
        catch (Exception e) {
            System.out.println("复制单个文件操作出错");
            e.printStackTrace();

        }

    }

    @Test
    public void test2(){
        fsStoreListService.initGoods();
    }

    @Test
    public void test3(){
        fsStoreListService.initStore();
    }


    /**
     * 递归获取某路径下的所有文件，文件夹，并输出
     */

    public static List<String> getFiles(String path) {
        List<String> fileNames = new ArrayList<>();
        File file = new File(path);
// 如果这个路径是文件夹
        if (file.isDirectory()) {
// 获取路径下的所有文件
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
// 如果还是文件夹 递归获取里面的文件 文件夹
                if (files[i].isDirectory()) {
                    System.out.println("目录：" + files[i].getPath());
                    getFiles(files[i].getPath());
                } else {
                    fileNames.add(files[i].getName());
                    System.out.println("文件：" + files[i].getPath());
                }
            }
        } else {
            fileNames.add(file.getName());
            System.out.println("文件：" + file.getPath());
        }
        return fileNames;
    }

    public static void  main    (String [] a){
        ZengshuEnum cid =  ZengshuEnum.getValue("佛山-金色领域一店-烟草证.pdf");
        String aa = "佛山-金色领域一店-烟草证.pdf";
        String [] arr = aa.split("\\.");
        String [] arrs = arr[0].split("-");
    }

}
