package com.uhome.file.uhomefilesystem.common.enums;

public enum  CertificateDeptEnum {
    SHANG_PIN_BU("1", "商品部"), ZHENG_WU_BU("2", "政务部");

    /**
     * 证件部门代码
     */
    private String code;

    /**
     * 证件部门名称
     */
    private String name;

    CertificateDeptEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
