package com.uhome.file.uhomefilesystem;

import com.uhome.file.uhomefilesystem.config.datasources.DynamicDataSourceConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableScheduling
@EnableTransactionManagement
@SpringBootApplication
@Import({DynamicDataSourceConfig.class})
@MapperScan("com.uhome.file.uhomefilesystem.*.dao")
public class UhomeFileSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(UhomeFileSystemApplication.class, args);
	}

}
