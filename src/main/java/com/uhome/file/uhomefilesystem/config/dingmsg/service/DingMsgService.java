package com.uhome.file.uhomefilesystem.config.dingmsg.service;


import com.uhome.file.uhomefilesystem.config.dingmsg.pojo.dto.DingMsgDto;
import com.uhome.file.uhomefilesystem.db.vo.ProductDetailDingVo;
import com.uhome.file.uhomefilesystem.db.vo.StoreDetailDingVo;

/**
 * Created by uh on 2020/7/15.
 */
public interface DingMsgService {

    void sendDingMsg(DingMsgDto dingMsgDto);

    void sendStoreRemindTime(StoreDetailDingVo vo);

    void sendProductRemindTime(ProductDetailDingVo vo);

    void sendStoreErrContext(StoreDetailDingVo vo);

    void sendProductErrContext(ProductDetailDingVo vo);
}
