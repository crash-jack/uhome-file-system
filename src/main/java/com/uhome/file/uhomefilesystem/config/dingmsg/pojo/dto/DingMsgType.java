package com.uhome.file.uhomefilesystem.config.dingmsg.pojo.dto;

/**
 * Created by uh on 2020/7/16.
 */
public enum DingMsgType {

    HANDLED, // 需处理
    SUCCEED, // 成功
    RETURNED // 退回

}
