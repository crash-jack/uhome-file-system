package com.uhome.file.uhomefilesystem.config.utils;


/**
 *  返回结果封装类，带有code，用于后台
 * @Author lgc
 * @Date 2019/4/29 11:01
 **/
public class ResultCode {
    private static final long serialVersionUID = 1L;

    private boolean success;
    private int code;

    private String message;

    private Object data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ResultCode() {
        this.code = ResultCodeEnum.Error.getKey();
        this.message = ResultCodeEnum.Error.getValue();
    }

    public ResultCode(int code) {
        this.code = code;
        this.message = ResultCodeEnum.getValue(this.code);
    }

    public ResultCode(ResultCodeEnum code) {
        this.code = code.getKey();
        this.success = code.getKey()==0?true:false;
        this.message = code.getValue();
    }

    public int getCode() {
        return code;
    }

    public ResultCode setCode(ResultCodeEnum code) {
        this.code = code.getKey();
        this.message = code.getValue();
        return this;
    }

    public ResultCode setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResultCode setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        if(data == null) {
            data = "";
        }
        return data;
    }

    public ResultCode setData(Object data) {
        this.data = data;
        return this;
    }


    public static ResultCode ok() {
        ResultCode result = new ResultCode();
        result.setCode(0);
        result.setSuccess(true);
        result.setMessage(ResultCodeEnum.getValue(0));
        return result;
    }

    public static ResultCode ok(String message) {
        ResultCode result = new ResultCode();
        result.setCode(0);
        result.setSuccess(true);
        result.setMessage(message);
        return result;
    }

    public static ResultCode error() {
        ResultCode result = new ResultCode();
        result.setCode(1);
        result.setSuccess(false);
        result.setMessage(ResultCodeEnum.getValue(1));
        return result;
    }

    public static ResultCode returnEnum(ResultCodeEnum reulstCodeEnum) {
        ResultCode result = new ResultCode();
        result.setSuccess(false);
        result.setCode(reulstCodeEnum.getKey());
        result.setMessage(reulstCodeEnum.getValue());
        return result;
    }


    public static ResultCode error(String message) {
        ResultCode result = new ResultCode();
        result.setCode(1);
        result.setSuccess(false);
        result.setMessage(message);
        return result;
    }

    public static ResultCode error(int code, String message) {
        ResultCode result = new ResultCode();
        result.setCode(code);
        result.setSuccess(false);
        result.setMessage(message);
        return result;
    }

}
