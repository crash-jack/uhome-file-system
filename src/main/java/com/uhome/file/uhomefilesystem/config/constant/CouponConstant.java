package com.uhome.file.uhomefilesystem.config.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "coupon.constant")
@PropertySource("classpath:coupon-constant.yml")
public class CouponConstant {
    @Value("${redis_expire_seconds}")
    private Long redis_expire_seconds;

    public Long getRedis_expire_seconds() {
        return redis_expire_seconds;
    }

    public void setRedis_expire_seconds(Long redis_expire_seconds) {
        this.redis_expire_seconds = redis_expire_seconds;
    }
}
