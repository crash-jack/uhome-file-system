package com.uhome.file.uhomefilesystem.config.filter;

import com.uhome.file.uhomefilesystem.db.service.IRedisService;
import com.uhome.file.uhomefilesystem.db.service.impl.RedisServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by uh on 2018/9/14.
 */
@Component
public class LoginFilter implements Filter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private IRedisService redisService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        redisService = (RedisServiceImpl) ctx.getBean("redisService");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");
        logger.info("path:" + path);

        if (path.contains("/swagger")
                || (path.endsWith("/api/h5/query_product_data"))
                || (path.endsWith("/api/h5/query_store_data"))
                || (path.endsWith("/api/pc/sys/qrLogin"))
                || (path.endsWith("/api/init/sys/initStore"))
                || (path.endsWith("/api/init/sys/initStoreImg"))
                || (path.endsWith("/api/init/sys/initGoods"))
                || (path.endsWith("/api/init/sys/initGoodsImg"))
                || (path.endsWith("/api/pc/sys/getUserPhone"))
                || (path.endsWith("/api/pc/h5/qrLogin"))
                || (path.endsWith("/api/h5/message/reportErr"))
                || (path.endsWith("/api/pc/download/file"))
                || (path.endsWith("/api/pc/download/batch_product"))
                || (path.endsWith("/api/pc/download/batch_store"))
                || (path.contains("/api/pc/"))
                || (path.endsWith("/api/h5/query/productLog"))
                || (path.endsWith("/api/h5/query/storeLog"))
                || path.endsWith(".js")
                || path.endsWith(".css")
                || path.endsWith(".png")
                || path.endsWith(".jpg")
                || path.endsWith(".jpeg")
                || path.endsWith(".gif")
                || path.endsWith(".html")
                || path.endsWith(".ico")
                || path.indexOf("/st/excel") != -1
                || path.endsWith(".xls")
                || path.endsWith(".xlsx")) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            String token = request.getHeader("token");
            logger.debug("request.getHeader.token:" + token);
            if (token == null || "".equals(token.trim())) {
                token = (String) request.getAttribute("token");
                logger.debug("request.getAttribute.token:" + token);
            }
            if (token == null || "".equals(token.trim())) {
                token = request.getParameter("token");
                logger.debug("request.getParameter.token:" + token);
            }

            if (token == null || "".equals(token)) {
                logger.debug("token在请求中不存在");
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return;
            }

            //获取redis中的Token
            String userName  = redisService.get(token) == null ? null : (String) redisService.get(token);
            if(userName == null){
                logger.debug("token在redis中不存在");
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
            //更新token时间为2个小时后失效
            redisService.put(token, userName, 60 * 60 * 2);
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}