package com.uhome.file.uhomefilesystem.config.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.uhome.file.uhomefilesystem.config.constant.RecordTypeConstant;
import com.uhome.file.uhomefilesystem.config.constant.StoreStateTypeConstant;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

public class ExportDataUtils {
    public static void setWriterResponse(HttpServletResponse response, String fileName) {
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setCharacterEncoding("UTF-8");
        try {
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8") + ".xls");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static String exportTypeToData(int type) {
        Date date = DateUtil.date();
        Date toDate;
        if (type == 0) {
            // 0-近3个月
            toDate = DateUtil.offsetMonth(date, -3);
        } else if (type == 1) {
            // 1-近半年
            toDate = DateUtil.offsetMonth(date, -6);
        } else if (type == 2) {
            // 2-近一年
            toDate = DateUtil.offsetMonth(date, -12);
        } else {
            toDate = date;
        }
        return DateUtil.formatDateTime(toDate);
    }

    public static String getRecordType(String recordType) {
        String type = null;
        if (recordType.equals(RecordTypeConstant.NOT_ENTERED.toString())) {
            type = "未录入";
        } else if (recordType.equals(RecordTypeConstant.PARTIAL_ENTRY.toString())) {
            type = "部分录入";
        } else if (recordType.equals(RecordTypeConstant.ENTERED.toString())) {
            type = "已录入";
        }
        return type;
    }

    public static String getStoreType(String storeType) {
        String type = null;
        if (StrUtil.isNotEmpty(storeType)) {
            if (storeType.equals("0")) {
                type = "个人";
            } else if (storeType.equals("1")) {
                type = "公司";
            }
        }
        return type;
    }

    public static String getStoreState(String storeState) {
        String type = null;
        if (storeState.equals(StoreStateTypeConstant.OPEN.toString())) {
            type = "开店";
        } else if (storeState.equals(StoreStateTypeConstant.CLOSED.toString())) {
            type = "关店";
        }
        return type;
    }

    public static String getProductState(String state) {
        String type = null;
        switch (state) {
            case "1":
                type = "正常";
                break;
            case "2":
                type = "下架";
                break;
            case "3":
                type = "待上架";
                break;
        }
        return type;
    }
}
