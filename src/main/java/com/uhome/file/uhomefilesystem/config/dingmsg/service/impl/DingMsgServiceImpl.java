package com.uhome.file.uhomefilesystem.config.dingmsg.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.uhome.file.uhomefilesystem.config.dingmsg.pojo.dto.DingMsgDto;
import com.uhome.file.uhomefilesystem.config.dingmsg.service.DingMsgService;
import com.uhome.file.uhomefilesystem.db.vo.ProductDetailDingVo;
import com.uhome.file.uhomefilesystem.db.vo.StoreDetailDingVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by uh on 2020/7/15.
 */
@Service
public class DingMsgServiceImpl implements DingMsgService {

    private static Logger logger = LoggerFactory.getLogger(DingMsgServiceImpl.class);

    public static final String DING_MSG_URL = "http://bi.uh24.com.cn/coin/msg/sendDingMsg";

//    @Autowired
//    private RestTemplate httpClientTemplate;

    /**
     * 发送钉钉消息
     */
    @Override
    public void sendDingMsg(DingMsgDto dingMsgDto) {
        try {
            String body = HttpRequest.post(DING_MSG_URL).body(JSONUtil.toJsonStr(dingMsgDto)).timeout(20000).execute().body();
            //ResponseEntity responseEntity = this.httpClientTemplate.postForEntity(DING_MSG_URL, dingMsgDto, String.class);
            logger.info("发送钉钉消息：{}，响应内容：{}", JSONUtil.toJsonStr(dingMsgDto), JSONUtil.toJsonStr(body));
        } catch (Exception e) {
            logger.error("send ding msg error.");
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public void sendStoreRemindTime(StoreDetailDingVo vo) {
        String title = "门店证件到期提醒";
        String msg = "**门店证件到期提醒。**";
        msg += "  \n  门店名称：" + vo.getStoreName();
        msg += "  \n  城市：" + vo.getCity();
        msg += "  \n  地址：" + vo.getAddress();
        msg += "  \n  店长姓名：" + vo.getManagerName();
        msg += "  \n  证件名称：" + vo.getDetailName();
        msg += "  \n  提醒时间：" + DateUtil.formatDateTime(vo.getRemindTime());
        DingMsgDto dingMsgDto = new DingMsgDto();
        dingMsgDto.setDdUserId(vo.getDdUserId());
        dingMsgDto.setMsg(msg);
        dingMsgDto.setTitle(title);
        sendDingMsg(dingMsgDto);
    }

    @Override
    public void sendProductRemindTime(ProductDetailDingVo vo) {
        String title = "商品证件到期提醒";
        String msg = "**商品证件到期提醒。**";
        msg += "  \n  商品名称：" + vo.getProductName();
        msg += "  \n  商品编码：" + vo.getProductCode();
        msg += "  \n  商品大类：" + vo.getProductCate1Name();
        msg += "  \n  商品中类：" + vo.getProductCate2Name();
        msg += "  \n  证件名称：" + vo.getDetailName();
        msg += "  \n  提醒时间：" + DateUtil.formatDateTime(vo.getRemindTime());
        DingMsgDto dingMsgDto = new DingMsgDto();
        dingMsgDto.setDdUserId(vo.getDdUserId());
        dingMsgDto.setMsg(msg);
        dingMsgDto.setTitle(title);
        sendDingMsg(dingMsgDto);
    }

    @Override
    public void sendStoreErrContext(StoreDetailDingVo vo) {
        String title = "门店证件错误提醒";
        String msg = "**门店证件错误提醒。**";
        msg += "  \n  门店名称：" + vo.getStoreName();
        msg += "  \n  城市：" + vo.getCity();
        msg += "  \n  地址：" + vo.getAddress();
        msg += "  \n  店长姓名：" + vo.getManagerName();
        msg += "  \n  时间：" + DateUtil.now();
        DingMsgDto dingMsgDto = new DingMsgDto();
        dingMsgDto.setDdUserId(vo.getDdUserId());
        dingMsgDto.setMsg(msg);
        dingMsgDto.setTitle(title);
        sendDingMsg(dingMsgDto);
    }

    @Override
    public void sendProductErrContext(ProductDetailDingVo vo) {
        String title = "商品证件错误提醒";
        String msg = "**商品证件错误提醒。**";
        msg += "  \n  商品名称：" + vo.getProductName();
        msg += "  \n  商品编码：" + vo.getProductCode();
        msg += "  \n  商品大类：" + vo.getProductCate1Name();
        msg += "  \n  商品中类：" + vo.getProductCate2Name();
        msg += "  \n  时间：" + DateUtil.now();
        DingMsgDto dingMsgDto = new DingMsgDto();
        dingMsgDto.setDdUserId(vo.getDdUserId());
        dingMsgDto.setMsg(msg);
        dingMsgDto.setTitle(title);
        sendDingMsg(dingMsgDto);
    }
}
