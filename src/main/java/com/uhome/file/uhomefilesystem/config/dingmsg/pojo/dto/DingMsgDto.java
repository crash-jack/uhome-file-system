package com.uhome.file.uhomefilesystem.config.dingmsg.pojo.dto;

import lombok.Data;

/**
 * Created by uh on 2020/7/15.
 */
@Data
public class DingMsgDto {

    private String ddUserId;

    private String title;

    private String msg;


    public String getDdUserId() {
        return ddUserId;
    }

    public void setDdUserId(String ddUserId) {
        this.ddUserId = ddUserId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
