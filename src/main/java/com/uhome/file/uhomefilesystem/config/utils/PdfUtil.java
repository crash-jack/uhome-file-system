package com.uhome.file.uhomefilesystem.config.utils;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Component
public class PdfUtil {


    @Value("${upload.imagesAddress}")
    private String imagesAddress;

    private static String imgPath;

    @PostConstruct
    public void getImgPath() {
        imgPath = this.imagesAddress;
    }

    /***
     * PDF文件转PNG图片，返回第一页的png图片，这个方法比下面的方法略快一点
     *
     * @param pdfFilePath 相对路径
     * @param dpi dpi越大转换后越清晰，相对转换速度越慢
     * @return 返回png路径，在原pdf名称后面加 _1.png
     */
    public static String pdfToPng(String pdfFilePath, int dpi)   {
        File file = new File(imgPath + pdfFilePath);
//        File file = new File( pdfFilePath);
        PDDocument pdDocument;
        String imgPdfPath = file.getParent();
        int dot = file.getName().lastIndexOf('.');
        // 获取图片文件名
        try {
            String imagePdfName = file.getName().substring(0, dot);
            pdDocument = PDDocument.load(file);
            try {
                PDFRenderer renderer = new PDFRenderer(pdDocument);
                StringBuffer imgFilePath;
                String imgFilePathPrefix = imgPdfPath + File.separator + imagePdfName;
                imgFilePath = new StringBuffer();
                imgFilePath.append(imgFilePathPrefix);
                imgFilePath.append("_1");
                imgFilePath.append(".png");
                File dstFile = new File(imgFilePath.toString());
                BufferedImage image = renderer.renderImageWithDPI(0, dpi);
                ImageIO.write(image, "png", dstFile);
                log.info("PDF文档转PNG图片成功！");
                return dstFile.getAbsolutePath().replace(imgPath, "");
            } catch (IOException e) {
                log.error(e.getMessage(), e);
                e.printStackTrace();
            } finally {
                pdDocument.close();
            }
        }catch (Exception e){
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }

    /***
     * PDF文件转PNG图片
     * @param PdfFilePath pdf相对路径  /filesystem/2021/8/25/5c0da2e8157c44f6b8ca37d5f17382a9.pdf
     * @param dstImgFolder 图片存放的文件夹  不传则生成png图片和pdf文件在同一目录
     * @param dpi dpi越大转换后越清晰，相对转换速度越慢
     * @param flag 页数 为0则转换全部页数
     * @return
     */
    public static void pdf2Image(String PdfFilePath, String dstImgFolder, int dpi, int flag) {
        // 原pdf文件
        File file = new File(imgPath + PdfFilePath);
        // 如果不是pdf，不处理
        String name = file.getName();
        if (StrUtil.isBlank(name) && !name.endsWith(".pdf")) {
            return;
        }
        PDDocument pdDocument;
        try {
            String imgPDFPath = file.getParent();
            int dot = file.getName().lastIndexOf('.');
            String imagePDFName = file.getName().substring(0, dot);
            String imgFolderPath = null;
            if (dstImgFolder.equals("")) {
                imgFolderPath = imgPDFPath;
            } else {
                imgFolderPath = dstImgFolder;
            }

            if (createDirectory(imgFolderPath)) {
                pdDocument = PDDocument.load(file);

                PDFRenderer renderer = new PDFRenderer(pdDocument);
                int pages = pdDocument.getNumberOfPages();
                // 大于0则打印具体页数
                if (flag > 0) {
                    if (flag < pages) {
                        pages = flag;
                    }
                }

                StringBuffer imgFilePath = null;
                for (int i = 0; i < pages; i++) {
                    String imgFilePathPrefix = imgFolderPath + File.separator + imagePDFName;
                    imgFilePath = new StringBuffer();
                    imgFilePath.append(imgFilePathPrefix);
                    imgFilePath.append("_");
                    imgFilePath.append(i + 1);
                    imgFilePath.append(".png");
                    File dstFile = new File(imgFilePath.toString());
                    BufferedImage image = renderer.renderImageWithDPI(i, dpi);
                    ImageIO.write(image, "png", dstFile);
                }
                System.out.println("success");
            } else {
                log.error("error:" + "creat" + imgFolderPath + "wrong");
                System.out.println("error:" + "creat" + imgFolderPath + "wrong");
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
    }

    private static boolean createDirectory(String folder) {
        File dir = new File(folder);
        if (dir.exists()) {
            return true;
        } else {
            return dir.mkdirs();
        }
    }

    public static void asynTransPDFToPNG(String img, int dpi) {
        CompletableFuture.supplyAsync(() -> pdfToPng(img, dpi));
    }
}