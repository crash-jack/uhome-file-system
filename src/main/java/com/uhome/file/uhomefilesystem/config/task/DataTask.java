package com.uhome.file.uhomefilesystem.config.task;

import cn.hutool.core.collection.CollUtil;
import com.uhome.file.uhomefilesystem.config.dingmsg.service.DingMsgService;
import com.uhome.file.uhomefilesystem.db.service.IFsProductDetilService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreDetailService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreListService;
import com.uhome.file.uhomefilesystem.db.vo.ProductDetailDingVo;
import com.uhome.file.uhomefilesystem.db.vo.StoreDetailDingVo;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class DataTask {

    private final IFsStoreDetailService iFsStoreDetailService;
    private final IFsStoreListService fsStoreListService;
    private final IFsProductDetilService iFsProductDetilService;
    private final DingMsgService dingMsgService;

}
