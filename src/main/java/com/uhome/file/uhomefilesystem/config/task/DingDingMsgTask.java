package com.uhome.file.uhomefilesystem.config.task;

import cn.hutool.core.collection.CollUtil;
import com.uhome.file.uhomefilesystem.config.dingmsg.service.DingMsgService;
import com.uhome.file.uhomefilesystem.db.service.IFsProductDetilService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreDetailService;
import com.uhome.file.uhomefilesystem.db.vo.ProductDetailDingVo;
import com.uhome.file.uhomefilesystem.db.vo.StoreDetailDingVo;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class DingDingMsgTask {

    private final IFsStoreDetailService iFsStoreDetailService;
    private final IFsProductDetilService iFsProductDetilService;
    private final DingMsgService dingMsgService;

    @Scheduled(cron = "${ding.remind-time}")
    public void execute() {
        //发送门店消息
        List<StoreDetailDingVo> storeList = iFsStoreDetailService.getListByRemindTime();
        //发送商品消息
        List<ProductDetailDingVo> productList = iFsProductDetilService.getListByRemindTime();

        if (CollUtil.isNotEmpty(storeList)) {
            storeList.forEach(item -> {
                if (item.getDdUserId() != null) {
                    dingMsgService.sendStoreRemindTime(item);
                }
            });
        }

        if (CollUtil.isNotEmpty(productList)) {
            productList.forEach(item -> {
                if (item.getDdUserId() != null) {
                    dingMsgService.sendProductRemindTime(item);
                }
            });
        }
    }
}
