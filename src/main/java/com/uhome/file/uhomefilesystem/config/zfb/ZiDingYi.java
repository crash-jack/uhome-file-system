package com.uhome.file.uhomefilesystem.config.zfb;

import com.alibaba.fastjson.JSONObject;
import com.uhome.file.uhomefilesystem.config.utils.HttpUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ZiDingYi {

    public static String img_base64(String path) {
        /**
         *  对path进行判断，如果是本地文件就二进制读取并base64编码，如果是url,则返回
         */
        String imgBase64="";
        if (path.startsWith("http")){
            imgBase64 = path;
        }else {
            try {
                File file = new File(path);
                byte[] content = new byte[(int) file.length()];
                FileInputStream finputstream = new FileInputStream(file);
                finputstream.read(content);
                finputstream.close();
                imgBase64 = new String(Base64.encodeBase64(content));
            } catch (IOException e) {
                e.printStackTrace();
                return imgBase64;
            }
        }

        return imgBase64;
    }

//    public static void main(String[] args) {
//        String host = "https://ocrdiy.market.alicloudapi.com";
//        String path = "/api/predict/ocr_sdt";
//        String method = "POST";
//        String appcode = "f4aa891799bb431eaea3658d0deea8cf";
//        Map<String, String> headers = new HashMap<String, String>();
//
//        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
//        headers.put("Authorization", "APPCODE " + appcode);
//        //根据API的要求，定义相对应的Content-Type
//        headers.put("Content-Type", "application/json; charset=UTF-8");
//        Map<String, String> querys = new HashMap<String, String>();
////        String bodys = "输入格式(多模板自动匹配):基于关键字可以输入一组template_id，每组template_id有对应的条件(cond),满足条件即使用该template_id基本条件包含include，exclude，分别表示包含/不包含某个关键字组合条件使用and,or表示，详见下面的示例{\"image\":\"图片二进制数据的base64编码\",\"configure\":{\"template_list\":[{\"template_id\":\"fc7b375c-8e98-48ef-81fb-ec3843b2fb371534902155\",#星巴克的模板id\"cond\":{\"and\":[{\"or\":[{\"include\":\"星巴克\"},{\"include\":\"starbuck\"}]},{\"exclude\":\"Costa\"}]}},{\"template_id\":\"7bc26bd7-41ad-4145-9f5a-4af21b26de9a\",#costa的模板id\"cond\":{\"include\":\"costa\"}}]}}自动匹配{\"image\":\"图片二进制数据的base64编码\",\"configure\":{\"template_list\":[\"fc7b375c-8e98-48ef-81fb-ec3843b2fb371534902155\",#星巴克的模板id\"7bc26bd7-41ad-4145-9f5a-4af21b26de9a\",#costa的模板id]}}输出格式(不带表格的):{\"config_str\":\"{\\\"template_id\\\":\\\"95d551ee-8b2b-4ad0-89a9-ed13417f4a781536146904\\\"}\",\"items\":{\"cash_id\":\":11535001\",#key-value组合，key是用户在界面上填写的\"cash_name\":\":李伟\",\"change\":\"￥0.00\",\"date\":\":2018.03.1616:46\",\"drop\":\"￥-0.10\",\"num\":\":2\",\"pay\":\"￥65.50\",\"sell_type\":\"外带\",\"seller\":\"24662\",\"shop_name\":\"TEL:0571-5697229\",\"total\":\"￥65.60\"},\"request_id\":\"20180723151536_fa0edbe7408fc589b93119f7a53a1260\",\"success\":true}输出格式(带表格的输出)：{\"config_str\":\"{\\\"template_id\\\":\\\"dd3ada93-c6ed-4427-8e0b-fbdebad614061532347130\\\"}\",\"items\":{\"item_no\":\"1313131313\",\"mail\":\"daigou@gmail.com\",\"name\":\"代购小哥\",\"nation\":\"美国\",\"phone\":\"987654321\",\"table0\":{#table0是用户在界面上填写的表格名称#labels是用户在界面上填写的，表格每一列的名称,values是每一行的值#一行的数组和labels是一一对应的关系“labels\":[\"brand\",\"model\",\"name\",\"quantity\",\"standard\",\"total\"]\"values\":[[\"Iphone\",\"8p\",\"手机\",\"3\",\"128G\",\"15000rmb\"],[\"戴森\",\"V8\",\"吸尘器\",\"1\",\"Absolute\",\"3500元\"],[\"\",\"\",\"\",\"\",\"\",\"\"],[\"\",\"\",\"\",\"\",\"\",\"\"],[\"\",\"\",\"\",\"\",\"\",\"\"]]}}}";
////        String imgFile = "D:\\Image\\自定义表格\\mmexport1604480816016.jpg";
////        String imgFile = "D:\\Image\\自定义表格\\u=2621735962,1104521659&fm=26&gp=0.jpg";
////        String imgFile = "D:\\Image\\身份证\\5d4a6a6cc12d7f6a098107cb3309f74d.jpg";
//        String imgFile = "C:\\Users\\admin\\Desktop\\微信图片_20210519054737.jpg";
//
//        String imageBase = img_base64(imgFile);
//
////        String bodys = "{\"image\":\""+ imageBase + "\",\"configure\":{\"template_list\":[{\"template_id\":\"40a74911-ea52-42b3-b2df-07ec03cd0da91603781845\",\"cond\":{\"or\":[{\"include\":\"姓名\"},{\"include\":\"性别\"}]}},{\"template_id\":\"25850085-6c33-4ab3-a13c-fa672fcc781d1603960559\"}]}}";
////        String bodys = "{\"image\":\""+ imageBase + "\",\"configure\":{\"template_list\":[{\"template_id\":\"25850085-6c33-4ab3-a13c-fa672fcc781d1603960559\",\"cond\":{\"or\":[{\"include\":\"代码\"},{\"include\":\"成交\"}]}},{\"template_id\":\"40a74911-ea52-42b3-b2df-07ec03cd0da91603781845\"}]}}";
//
//
//
//
//        JSONObject req = new JSONObject();
//        JSONObject configObj = new JSONObject();
//        List<String> list = new ArrayList<String>();
//        list.add("bbcd4d9b-88e6-4a15-bb65-a2f80f5971ad1626337498");
//
//        configObj.put("template_list",list);
//
////        configObj.put("rotate",true);
//        req.put("image",imageBase);
////        req.put("image","https://oss-test-edu.oss-cn-hangzhou.aliyuncs.com/0.png?versionId=CAEQJxiBgMC.pP_uuhciIDgwNzNhNzg5NDJkODRmYTFiOThiZDNhZTBjNTNhOTBi");
//        req.put("configure",configObj);
//
//        String bodys =req.toString();
//        System.out.println(bodys);
//        try {
//            /**
//             * 重要提示如下:
//             * HttpUtils请从
//             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
//             * 下载
//             *
//             * 相应的依赖请参照
//             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
//             */
//            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
//            System.out.println(response.toString());
//            //获取response的body
//            System.out.println(EntityUtils.toString(response.getEntity()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    public String   getBack(String url,String tempId) {
        String host = "https://ocrdiy.market.alicloudapi.com";
        String path = "/api/predict/ocr_sdt";
        String method = "POST";
        String appcode = "f4aa891799bb431eaea3658d0deea8cf";
        Map<String, String> headers = new HashMap<String, String>();

        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        //根据API的要求，定义相对应的Content-Type
        headers.put("Content-Type", "application/json; charset=UTF-8");
        Map<String, String> querys = new HashMap<String, String>();
//        String bodys = "输入格式(多模板自动匹配):基于关键字可以输入一组template_id，每组template_id有对应的条件(cond),满足条件即使用该template_id基本条件包含include，exclude，分别表示包含/不包含某个关键字组合条件使用and,or表示，详见下面的示例{\"image\":\"图片二进制数据的base64编码\",\"configure\":{\"template_list\":[{\"template_id\":\"fc7b375c-8e98-48ef-81fb-ec3843b2fb371534902155\",#星巴克的模板id\"cond\":{\"and\":[{\"or\":[{\"include\":\"星巴克\"},{\"include\":\"starbuck\"}]},{\"exclude\":\"Costa\"}]}},{\"template_id\":\"7bc26bd7-41ad-4145-9f5a-4af21b26de9a\",#costa的模板id\"cond\":{\"include\":\"costa\"}}]}}自动匹配{\"image\":\"图片二进制数据的base64编码\",\"configure\":{\"template_list\":[\"fc7b375c-8e98-48ef-81fb-ec3843b2fb371534902155\",#星巴克的模板id\"7bc26bd7-41ad-4145-9f5a-4af21b26de9a\",#costa的模板id]}}输出格式(不带表格的):{\"config_str\":\"{\\\"template_id\\\":\\\"95d551ee-8b2b-4ad0-89a9-ed13417f4a781536146904\\\"}\",\"items\":{\"cash_id\":\":11535001\",#key-value组合，key是用户在界面上填写的\"cash_name\":\":李伟\",\"change\":\"￥0.00\",\"date\":\":2018.03.1616:46\",\"drop\":\"￥-0.10\",\"num\":\":2\",\"pay\":\"￥65.50\",\"sell_type\":\"外带\",\"seller\":\"24662\",\"shop_name\":\"TEL:0571-5697229\",\"total\":\"￥65.60\"},\"request_id\":\"20180723151536_fa0edbe7408fc589b93119f7a53a1260\",\"success\":true}输出格式(带表格的输出)：{\"config_str\":\"{\\\"template_id\\\":\\\"dd3ada93-c6ed-4427-8e0b-fbdebad614061532347130\\\"}\",\"items\":{\"item_no\":\"1313131313\",\"mail\":\"daigou@gmail.com\",\"name\":\"代购小哥\",\"nation\":\"美国\",\"phone\":\"987654321\",\"table0\":{#table0是用户在界面上填写的表格名称#labels是用户在界面上填写的，表格每一列的名称,values是每一行的值#一行的数组和labels是一一对应的关系“labels\":[\"brand\",\"model\",\"name\",\"quantity\",\"standard\",\"total\"]\"values\":[[\"Iphone\",\"8p\",\"手机\",\"3\",\"128G\",\"15000rmb\"],[\"戴森\",\"V8\",\"吸尘器\",\"1\",\"Absolute\",\"3500元\"],[\"\",\"\",\"\",\"\",\"\",\"\"],[\"\",\"\",\"\",\"\",\"\",\"\"],[\"\",\"\",\"\",\"\",\"\",\"\"]]}}}";
//        String imgFile = "D:\\Image\\自定义表格\\mmexport1604480816016.jpg";
//        String imgFile = "D:\\Image\\自定义表格\\u=2621735962,1104521659&fm=26&gp=0.jpg";
//        String imgFile = "D:\\Image\\身份证\\5d4a6a6cc12d7f6a098107cb3309f74d.jpg";
        String imgFile =url;

        String imageBase = img_base64(imgFile);

//        String bodys = "{\"image\":\""+ imageBase + "\",\"configure\":{\"template_list\":[{\"template_id\":\"40a74911-ea52-42b3-b2df-07ec03cd0da91603781845\",\"cond\":{\"or\":[{\"include\":\"姓名\"},{\"include\":\"性别\"}]}},{\"template_id\":\"25850085-6c33-4ab3-a13c-fa672fcc781d1603960559\"}]}}";
//        String bodys = "{\"image\":\""+ imageBase + "\",\"configure\":{\"template_list\":[{\"template_id\":\"25850085-6c33-4ab3-a13c-fa672fcc781d1603960559\",\"cond\":{\"or\":[{\"include\":\"代码\"},{\"include\":\"成交\"}]}},{\"template_id\":\"40a74911-ea52-42b3-b2df-07ec03cd0da91603781845\"}]}}";




        JSONObject req = new JSONObject();
        JSONObject configObj = new JSONObject();
        List<String> list = new ArrayList<String>();
        list.add(tempId);
        configObj.put("template_list",list);

//        configObj.put("rotate",true);
        req.put("image",imageBase);
//        req.put("image","https://oss-test-edu.oss-cn-hangzhou.aliyuncs.com/0.png?versionId=CAEQJxiBgMC.pP_uuhciIDgwNzNhNzg5NDJkODRmYTFiOThiZDNhZTBjNTNhOTBi");
        req.put("configure",configObj);

        String bodys =req.toString();
        System.out.println(bodys);
        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            int stat = response.getStatusLine().getStatusCode();
            if(stat != 200){
                System.out.println("Http body error msg:" + EntityUtils.toString(response.getEntity()));
                return null;
            }else{
                System.out.println(response.toString());
                //获取response的body
//                System.out.println(EntityUtils.toString(response.getEntity()));
                return EntityUtils.toString(response.getEntity());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
