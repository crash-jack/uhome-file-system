package com.uhome.file.uhomefilesystem.config.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 城市名称类型
 */
public enum ZengshuEnum {
    SPZZ(1, "商品资质"),
    GYSZZ(2, "供应商资质"),
    YCZ(3, "烟草证"),
    SPZ(4, "食品证"),
    YYZZ(5, "营业执照");

    private final Integer key;
    private final String value;

    ZengshuEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public static ZengshuEnum getValue(String code){
              for (ZengshuEnum  color: values()) {
                     if(color.getValue().equals(code)){
                              return  color;
                         }
                   }
               return null;

           }


    public Integer getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    private static Map<Integer, ZengshuEnum> map = new HashMap<Integer, ZengshuEnum>();

    static {
        for (ZengshuEnum code : ZengshuEnum.values()) {
            map.put(code.key, code);
        }
    }

    public static String getValue(Integer key) {
        ZengshuEnum code = map.get(key);
        if( null != code ) {
            return code.value;
        }
        return null;
    }
    public static  Map<Integer, ZengshuEnum> getMap() {
        return map;
    }
}
