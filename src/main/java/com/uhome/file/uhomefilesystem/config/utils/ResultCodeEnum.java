package com.uhome.file.uhomefilesystem.config.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author lgc
 * @Date 2019/4/29 11:20
 **/
public enum ResultCodeEnum {

    Success(0, "成功"),
    Error(1, "失败"),
    NoPermission(401, "无权限"),
    Exception(1000, "异常"),
    NotAuthorized(2000, "没有授权"),
    TokenInvalid(203001, "token无效"),
    TxTimeout(3000, "事务超时回滚"),
    TxRequired(3001, "事务序号必填"),
    TxRepeat(3002, "事务序号重复"),
    TxRepeatCompleted(3003, "事务序号重复完成"),
    TxRollbackRepeat(3004, "回滚事务序号重复"),
    TxRollbackRepeatCompleted(3005, "回滚事务序号重复完成"),
    TxNotCompleted(3006, "事务序号没有完成"),
    LackOfStock(10001, "库存不足"),
    RequestGoods(10002, "申请商品有误"),
    LackOfCash(10003, "可提现金额不足"),
    AuthRealName(10004, "实名认证有误"),
    RepeatPayment(10005, "重复支付"),
    RepeatCreateSchoolStore(10006, "重复创建零食店"),
    LackOfAccount(10007, "账户余额不足"),
    UserNotExist(10008, "用户不存在"),
    RepeatSubmit(10009, "重复提交"),
    RequiredParam(10010, "参数必填"),
    RequestFail(40010, "第三方接口调用失败"),
    RequestReturnFail(40020, "第三方接口返回调用失败"),
    PleaseTryLater(40000, "系统繁忙，请稍后再试"),
    PleaseTryLaterService(40099, "系统繁忙，请稍后再试");

    private final int key;
    private final String value;

    ResultCodeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    private static Map<Integer, ResultCodeEnum> map = new HashMap<Integer, ResultCodeEnum>();

    static {
        for (ResultCodeEnum iResultCode : ResultCodeEnum.values()) {
            map.put(iResultCode.key, iResultCode);
        }
    }

    public static String getValue(int key) {
        ResultCodeEnum iResultCode = map.get(key);
        if( null != iResultCode ) {
            return iResultCode.value;
        }
        return null;
    }
}
