package com.uhome.file.uhomefilesystem.config.constant;

public interface StoreStateTypeConstant {

    /**
     * 门店状态 0.开店  1.关店
     */
    Integer OPEN = 0;

    Integer CLOSED = 1;
}
