package com.uhome.file.uhomefilesystem.config.constant;

public interface RecordTypeConstant {

    /**
     * 录入状态 0.未录入
     */
    Integer NOT_ENTERED = 0;

    /**
     * 1.部分录入
     */
    Integer PARTIAL_ENTRY = 1;

    /**
     * 2.已录入
     */
    Integer ENTERED = 2;
}
