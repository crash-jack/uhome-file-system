package com.uhome.file.uhomefilesystem.api;

import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.manager.HFileManage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/h5")
public class HFileController {

    @Autowired
    private HFileManage fileManage;

    @GetMapping("query_product_data")
    public ResultCode queryProductData(String queryStr){
        try{
            ResultCode result =  fileManage.queryProductData(queryStr);
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    @GetMapping("query_store_data")
    public ResultCode queryStoreData(String queryStr){
        try{
            ResultCode result =  fileManage.queryStoreData(queryStr);
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

}
