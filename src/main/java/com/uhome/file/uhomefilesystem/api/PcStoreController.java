package com.uhome.file.uhomefilesystem.api;

import com.github.pagehelper.PageInfo;
import com.uhome.file.uhomefilesystem.api.common.AbstractController;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.entity.SysUser;
import com.uhome.file.uhomefilesystem.db.manager.HFileManage;
import com.uhome.file.uhomefilesystem.db.manager.PcFileManage;
import com.uhome.file.uhomefilesystem.db.param.FsStoreDetailParam;
import com.uhome.file.uhomefilesystem.db.param.FsStoreListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/pc/store")
public class PcStoreController extends AbstractController {

    @Autowired
    private HFileManage fileManage;

    @Autowired
    private PcFileManage pcFileManage;

    @GetMapping("query_store_data")
    public ResultCode queryStoreData(FsStoreListParam storeListParam, PageInfo pageable) {
        try {
            ResultCode result = pcFileManage.queryStoreData(storeListParam, pageable);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }


    @GetMapping("save_store_data")
    public ResultCode saveStoreData(HttpServletRequest request, FsStoreDetailParam fsStoreDetailParam) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }

            ResultCode result = pcFileManage.saveStoreData(user, fsStoreDetailParam);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    @PostMapping("delete_store_data")
    public ResultCode deleteStoreData(@RequestBody FsStoreDetailParam param,HttpServletRequest request){
        SysUser user = getUser(request);
        if (user == null || user.getId() == null) {
            return ResultCode.error("请重新登录");
        }
        param.setUpdator(user.getId());
        return pcFileManage.deleteStoreData(param);
    }

}
