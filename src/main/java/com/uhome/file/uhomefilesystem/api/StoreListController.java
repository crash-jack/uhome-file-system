package com.uhome.file.uhomefilesystem.api;

import com.uhome.file.uhomefilesystem.api.common.AbstractController;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.entity.SysUser;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreListService;
import com.uhome.file.uhomefilesystem.db.vo.TotalDataVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/pc/storeList")
public class StoreListController extends AbstractController {

    private final IFsStoreListService iFsStoreListService;

    /**
     * 根据当前用户获取城市和区域数据
     */
    @GetMapping("getCurrUserCityAndArea")
    public ResultCode getCurrUserCityAndArea(HttpServletRequest request) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }

            ResultCode result = ResultCode.ok();
            result.setData(iFsStoreListService.getCurrUserCityAndArea(user));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 根据当前用户获取城市和区域数据
     */
    @GetMapping("getCurrUserCityAndAreaForPc")
    public ResultCode getCurrUserCityAndAreaForPc(HttpServletRequest request) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }

            ResultCode result = ResultCode.ok();
            result.setData(iFsStoreListService.getCurrUserCityAndAreaForPc(user));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 获取门店总数信息
     */
    @GetMapping("getTotalData")
    public ResultCode getTotalData(@RequestParam(value = "city", required = false) String city) {
        try {
            TotalDataVo vo = iFsStoreListService.getTotalData(city);
            ResultCode result = ResultCode.ok();
            result.setData(vo);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 区域完成度占比
     */
    @GetMapping("getAreaEnRate")
    public ResultCode getAreaEnRate(@RequestParam(value = "city", required = false) String city,
                                    @RequestParam(value = "area", required = false) String area,
                                    HttpServletRequest request) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }

            ResultCode result = ResultCode.ok();
            result.setData(iFsStoreListService.getAreaEnRate(user, city, area));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 获取城市/区域完成情况
     */
    @GetMapping("getCityEnRate")
    public ResultCode getCityEnRate(@RequestParam(value = "city", required = false) String city,
                                    HttpServletRequest request) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }

            ResultCode result = ResultCode.ok();
            result.setData(iFsStoreListService.getCityEnRate(user, city));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 导出数据
     *
     * @param type 0-近3个月 1-近半年 2-近一年
     */
    @GetMapping("export")
    public void export(@RequestParam int type, HttpServletRequest request, HttpServletResponse response) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                throw new RuntimeException("请重新登录");
            }

            iFsStoreListService.export(user, type, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
