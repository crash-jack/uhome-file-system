package com.uhome.file.uhomefilesystem.api.common;


import cn.hutool.json.JSONUtil;
import com.uhome.file.uhomefilesystem.config.constant.TokenPre;
import com.uhome.file.uhomefilesystem.db.entity.DimEmployeeInfo;
import com.uhome.file.uhomefilesystem.db.entity.SysUser;
import com.uhome.file.uhomefilesystem.db.service.IRedisService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller公共组件
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	IRedisService redisService;

	protected SysUser getUser(HttpServletRequest request) {
		 String  token = request.getHeader("token");
		if (StringUtils.isNotEmpty(token)) {
			if(token.startsWith(TokenPre.pc_token_pre)){
				String userInfo =  redisService.get(token)!=null?redisService.get(token).toString():null;
				if(StringUtils.isNotEmpty(userInfo)){
					return  JSONUtil.toBean(userInfo,SysUser.class);
				}else{
					return null;
				}
			}
			return null;
		}else{
			return  null;
		}
	}

	protected Long getUserId(HttpServletRequest request) {
		SysUser sysUser = getUser(request);
		if(sysUser!=null){
			return sysUser.getId();
		}else{
			return null;
		}
	}

	protected DimEmployeeInfo getH5User(HttpServletRequest request) {
		String  token = request.getHeader("token");
		if (StringUtils.isNotEmpty(token)) {
			if(token.startsWith(TokenPre.h5_token_pre)){
				String userInfo =  redisService.get(token)!=null?redisService.get(token).toString():null;
				if(StringUtils.isNotEmpty(userInfo)){
					return  JSONUtil.toBean(userInfo,DimEmployeeInfo.class);
				}else{
					return null;
				}
			}
			return null;
		}else{
			return  null;
		}
	}


}
