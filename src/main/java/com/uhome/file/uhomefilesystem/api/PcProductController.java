package com.uhome.file.uhomefilesystem.api;

import com.github.pagehelper.PageInfo;
import com.uhome.file.uhomefilesystem.api.common.AbstractController;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.entity.SysUser;
import com.uhome.file.uhomefilesystem.db.manager.PcFileManage;
import com.uhome.file.uhomefilesystem.db.param.FsProductDetailParam;
import com.uhome.file.uhomefilesystem.db.param.FsProductListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/pc/product")
public class PcProductController extends AbstractController {

    @Autowired
    private PcFileManage pcFileManage;

    @GetMapping("query_product_data")
    public ResultCode queryProductData(FsProductListParam productListParam, PageInfo pageable, HttpServletRequest request) {
        try {
            return pcFileManage.queryProductData(productListParam, pageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }


    @GetMapping("save_product_data")
    public ResultCode saveProductData(FsProductDetailParam fsProductDetailParam, HttpServletRequest request) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }
            fsProductDetailParam.setUpdator(user.getId());
            ResultCode result = pcFileManage.saveProductData(fsProductDetailParam);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    @PostMapping("delete_product_data")
    public ResultCode deleteProductData(@RequestBody FsProductDetailParam param, HttpServletRequest request){
        SysUser user = getUser(request);
        if (user == null || user.getId() == null) {
            return ResultCode.error("请重新登录");
        }
        param.setUpdator(user.getId());
        return pcFileManage.deleteProductData(param);
    }


}
