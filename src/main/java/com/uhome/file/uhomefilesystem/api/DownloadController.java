package com.uhome.file.uhomefilesystem.api;

import com.uhome.file.uhomefilesystem.db.param.DownloadParam;
import com.uhome.file.uhomefilesystem.db.service.IDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/pc/download")
public class DownloadController {

    @Autowired
    private IDownloadService downloadService;

    @GetMapping("/file")
    public void file(DownloadParam downloadParam, HttpServletResponse response) throws Exception {
        downloadService.file(downloadParam, response);
    }

    @GetMapping("/batch_product")
    public void batchProduct(String ids, HttpServletResponse response) throws Exception {
        System.out.println(ids);
        downloadService.batchProduct(ids, response);
    }

    @GetMapping("/batch_store")
    public void batchStore(String ids, HttpServletResponse response) throws Exception {
        downloadService.batchStore(ids, response);
    }
}
