package com.uhome.file.uhomefilesystem.api;

import com.uhome.file.uhomefilesystem.api.common.AbstractController;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.service.IFsProductListService;
import com.uhome.file.uhomefilesystem.db.vo.TotalDataVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/pc/productList")
public class ProductListController extends AbstractController {

    private final IFsProductListService iFsProductListService;

    /**
     * 根据当前用户获取城市和区域数据
     */
    @GetMapping("getCurrUserProductType")
    public ResultCode getCurrUserProductType() {
        try {
            ResultCode result = ResultCode.ok();
            result.setData(iFsProductListService.getCurrUserProductType());
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 根据当前用户获取城市和区域数据
     */
    @GetMapping("getCurrUserProductTypeTree")
    public ResultCode getCurrUserProductTypeTree() {
        try {
            ResultCode result = ResultCode.ok();
            result.setData(iFsProductListService.getCurrUserProductTypeTree());
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 获取商品总数信息
     */
    @GetMapping("getTotalData")
    public ResultCode getTotalData() {
        try {
            TotalDataVo vo = iFsProductListService.getTotalData();
            ResultCode result = ResultCode.ok();
            result.setData(vo);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 种类完成度占比
     */
    @GetMapping("getTypeEnRate")
    public ResultCode getTypeEnRate(@RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                                    @RequestParam(value = "name", required = false) String name) {
        try {
            ResultCode result = ResultCode.ok();
            result.setData(iFsProductListService.getTypeEnRate(type, name));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 获取商品完成情况
     */
    @GetMapping("getTypeRate")
    public ResultCode getTypeRate(@RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                                  @RequestParam(value = "name", required = false) String name) {
        try {
            ResultCode result = ResultCode.ok();
            result.setData(iFsProductListService.getTypeRate(type, name));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 导出数据
     *
     * @param type 0-近3个月 1-近半年 2-近一年
     */
    @GetMapping("export")
    public void export(@RequestParam int type, HttpServletResponse response) {
        try {
            iFsProductListService.export(type, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
