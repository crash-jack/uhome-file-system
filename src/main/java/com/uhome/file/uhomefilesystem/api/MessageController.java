package com.uhome.file.uhomefilesystem.api;

import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.service.IFsReportMsgService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/h5/message")
public class MessageController {

    private final IFsReportMsgService IFsReportMsgService;

    @PostMapping("reportErr")
    public ResultCode reportErr(@RequestBody Map<String, Object> map) {
        try {
            Integer integer = IFsReportMsgService.reportMsg(map);
            if (integer > 0) {
                return ResultCode.ok();
            } else {
                return ResultCode.error();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error();
        }
    }
}
