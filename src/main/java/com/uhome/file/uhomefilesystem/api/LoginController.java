package com.uhome.file.uhomefilesystem.api;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.manager.LoginManage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/pc")
public class LoginController {

    @Autowired
    private LoginManage loginManage;


    @RequestMapping(value = "/sys/qrLogin", method = RequestMethod.GET)
    public ResultCode qrLogin(String phone, HttpServletRequest request) {
        try{
            return loginManage.qrLogin(phone);
        }catch (Exception e){
            e.printStackTrace();
            return ResultCode.error();
        }
    }


    @RequestMapping(value = "/h5/qrLogin", method = RequestMethod.GET)
    public ResultCode h5Login(String phone, HttpServletRequest request) {
        try{
            return loginManage.h5Login(phone);
        }catch (Exception e){
            e.printStackTrace();
            return ResultCode.error();
        }
    }

    @RequestMapping(value = "/sys/getUserPhone", method = RequestMethod.GET)
    public ResultCode getUserPhone(String id) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("id", id);
        try {
            String resultMmber = HttpUtil.get("https://vip.uh24.com.cn/uhome-bigdata/auth/rest/scan/getUserPhone",  params,6000);
            JSONObject json = JSONObject.parseObject(resultMmber);
            return ResultCode.ok().setData( json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultCode.error();
    }
}
