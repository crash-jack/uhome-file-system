package com.uhome.file.uhomefilesystem.api;

import com.uhome.file.uhomefilesystem.config.constant.ZengshuEnum;
import com.uhome.file.uhomefilesystem.config.utils.PdfUtil;
import com.uhome.file.uhomefilesystem.db.entity.FsStoreDetail;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreListService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/init")
public class InitDataController {

    private final IFsStoreListService fsStoreListService;

    @Value("${upload.imagesAddress}")
    private String imagesAddress;

    private String [] zhengshu = {"营业执照","烟草证","食品证"};

    @RequestMapping(value = "/sys/initStore", method = RequestMethod.GET)
    public void initStore() {
        try{
            fsStoreListService.initStore();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/sys/initGoods", method = RequestMethod.GET)
    public void initGoods() {
        try{
            fsStoreListService.initGoods();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/sys/initGoodsImg", method = RequestMethod.POST)
    public void initGoodsImg() {
        try{
            fsStoreListService.initGoodsImg();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @RequestMapping(value = "/sys/initStoreImg", method = RequestMethod.POST)
    public void initStoreImg() {
        String path = "/filesystem/2021/moban/store";
        log.info("开始获取名称" );
        List<String> fileNames = getFiles(imagesAddress+path);
        log.info("获取名称" );
        try {
            List<String> errorList = new ArrayList<>();
            List<FsStoreDetail> saveData = new ArrayList<>();
            List<FsStoreDetail> pdfData = new ArrayList<>();
            for (int i = 0; i < fileNames.size(); i++) {
                String [] arrName = fileNames.get(i).split("\\.");
                String [] arr = arrName[0].split("-");
                //校验数据
                if(arr.length==3){
                    boolean arr3Flag = false;
                    for (int j = 0; j < zhengshu.length ; j++) {
                        if(arr[2].equals(zhengshu[j] )){
                            arr3Flag = true;
                        }
                    }
                    if(!arr3Flag){
                        errorList.add(fileNames.get(i));
                        continue;
                    }
                    ZengshuEnum cid =  ZengshuEnum.getValue(arr[2]);
                    if(cid==null){
                        errorList.add(fileNames.get(i));
                        continue;
                    }
                    FsStoreDetail fsStoreDetail = new FsStoreDetail();
                    fsStoreDetail.setName(fileNames.get(i));
                    fsStoreDetail.setCertificateId(Long.parseLong(cid.getKey().toString()));
                    fsStoreDetail.setImg(path + arrName[0] + arrName[1]);
                    if (arrName[1].equals("pdf") || arrName[1].equals("PDF")) {
                        pdfData.add(fsStoreDetail);
                        fsStoreDetail.setPngImg(path + arrName[0] + "_1.png");
                    }else{
                        fsStoreDetail.setPngImg(path + arrName[0] + arrName[1]);
                    }
                } else {
                    errorList.add(fileNames.get(i));
                    continue;
                }
            }
            //pdf转图片
            if (pdfData != null && pdfData.size() > 0) {
                for (int i = 0; i < pdfData.size(); i++) {
                    PdfUtil.asynTransPDFToPNG(pdfData.get(i).getImg(),30);
                    log.info("pdf转换成功：" + pdfData.get(i).getImg());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            log.info(e.getMessage());
        }
    }

    public static List<String> getFiles(String path) {
        List<String> fileNames = new ArrayList<>();
        File file = new File(path);
// 如果这个路径是文件夹
        if (file.isDirectory()) {
// 获取路径下的所有文件
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
// 如果还是文件夹 递归获取里面的文件 文件夹
                if (files[i].isDirectory()) {
                    getFiles(files[i].getPath());
                } else {
                    fileNames.add(files[i].getName());
                }
            }
        } else {
            fileNames.add(file.getName());
        }
        return fileNames;
    }

}
