package com.uhome.file.uhomefilesystem.api;

import com.uhome.file.uhomefilesystem.api.common.AbstractController;
import com.uhome.file.uhomefilesystem.db.service.IFsProductQueryService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/h5/query")
public class QueryLogController extends AbstractController {

    private final IFsProductQueryService iFsProductQueryService;
    private final IFsStoreQueryService iFsStoreQueryService;

    /**
     * @param type      0-查看 1-下载
     * @param productId 商品id
     */
    @GetMapping("productLog")
    public void productLog(@RequestParam(value = "type") Integer type,
                           @RequestParam(value = "productId") Long productId,
                           @RequestParam(value = "certificateId",required = false) Long certificateId) {
        try {
            iFsProductQueryService.productLog(type, productId, certificateId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param type    0-查看 1-下载
     * @param storeId 门店id
     */
    @GetMapping("storeLog")
    public void storeLog(@RequestParam("type") Integer type,
                         @RequestParam("storeId") Long storeId,
                         @RequestParam(value = "certificateId",required = false) Long certificateId) {
        try {
            iFsStoreQueryService.storeLog(type, storeId, certificateId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
