package com.uhome.file.uhomefilesystem.api;

import cn.hutool.core.util.StrUtil;
import com.uhome.file.uhomefilesystem.config.utils.PdfUtil;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.config.utils.UploadImage;
import com.uhome.file.uhomefilesystem.config.zfb.ZiDingYi;
import com.uhome.file.uhomefilesystem.db.service.IFsSysService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.util.unit.DataSize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.MultipartConfigElement;

/**
 * Demo class
 *
 * @author hyin
 * @date 2019/04/19
 */
@RestController
@RequestMapping("/api/pc/upload")
@Slf4j
public class UploadController {

    @Value("${upload.imagesAddress}")
    private String imagesAddress;

    @Autowired
    IFsSysService sysService;

    @PostMapping("uploadImage")
    protected String uploadImage(@RequestParam(value = "file", required = true) MultipartFile file){
        log.info("进入上传文件接口");
        log.info("imagesAddress为："+imagesAddress);
        String img = UploadImage.copyFile(file, imagesAddress);
        log.info("文件复制成功，路径为："+img);
        // 如果是pdf,将pdf转成png
        if(StrUtil.isNotBlank(img)){
            // 如果不是pdf，不处理
            String name = file.getOriginalFilename();
            if (StrUtil.isNotBlank(name) && name.endsWith(".pdf")) {
                PdfUtil.asynTransPDFToPNG(img,30);
            }
        }
        return img;
    }

    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setLocation(imagesAddress);
        factory.setMaxFileSize(DataSize.ofMegabytes(5));
        return factory.createMultipartConfig();
    }


    @GetMapping("getDetails")
    protected ResultCode getDetails(String url,Long certificateId){
        ZiDingYi ziDingYi = new ZiDingYi();
        if(StringUtils.isEmpty(url)|| certificateId==null ){
            return ResultCode.error();
        }
        String tempId = sysService.getTempId(certificateId);
        String result = null;
        if(StringUtils.isNotBlank(tempId)){
            result = ziDingYi.getBack(imagesAddress+url,tempId);
        }else{
            return ResultCode.error();
        }
        if(result!=null){
            return ResultCode.ok().setData(result);
        }else{
            return ResultCode.error();
        }
//        return UploadImage.copyFile(file,imagesAddress);
    }


}
