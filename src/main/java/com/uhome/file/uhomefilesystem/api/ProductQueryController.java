package com.uhome.file.uhomefilesystem.api;

import com.uhome.file.uhomefilesystem.api.common.AbstractController;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.service.IFsProductQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/pc/productQuery")
public class ProductQueryController extends AbstractController {

    private final IFsProductQueryService iFsProductQueryService;

    /**
     * 获取商品 查询店铺查询次数
     */
    @GetMapping("getTypeData")
    public ResultCode getTypeData(@RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                                  @RequestParam(value = "name", required = false) String name) {
        try {
            ResultCode result = ResultCode.ok();
            result.setData(iFsProductQueryService.getCountTotal(type, name));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 导出数据
     *
     * @param type 0-近3个月 1-近半年 2-近一年
     */
    @GetMapping("export")
    public void export(@RequestParam int type, HttpServletResponse response) {
        try {
            iFsProductQueryService.export(type, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
