package com.uhome.file.uhomefilesystem.api;

import com.uhome.file.uhomefilesystem.api.common.AbstractController;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.entity.SysUser;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/pc/storeQuery")
public class StoreQueryController extends AbstractController {

    private final IFsStoreQueryService iFsStoreQueryService;

    @GetMapping("getCountTotalByCertType")
    public ResultCode getCountTotalByCertType(@RequestParam(value = "city", required = false) String city,
                                              HttpServletRequest request) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }

            ResultCode result = ResultCode.ok();
            result.setData(iFsStoreQueryService.getCountTotalByCertType(user, city));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }


    /**
     * 获取城市/区域 查询店铺查询次数
     */
    @GetMapping("getCityData")
    public ResultCode getCityData(@RequestParam(value = "city", required = false) String city,
                                  HttpServletRequest request) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }

            ResultCode result = ResultCode.ok();
            result.setData(iFsStoreQueryService.getCountTotal(user, city));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    /**
     * 导出数据
     *
     * @param type 0-近3个月 1-近半年 2-近一年
     */
    @GetMapping("export")
    public void export(@RequestParam int type, HttpServletRequest request, HttpServletResponse response) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                throw new RuntimeException("请重新登录");
            }

            iFsStoreQueryService.export(user, type, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
