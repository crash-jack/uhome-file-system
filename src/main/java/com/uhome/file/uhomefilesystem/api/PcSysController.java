package com.uhome.file.uhomefilesystem.api;

import com.github.pagehelper.PageInfo;
import com.uhome.file.uhomefilesystem.api.common.AbstractController;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.dao.FsLoginLogMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsLoginLog;
import com.uhome.file.uhomefilesystem.db.entity.SysUser;
import com.uhome.file.uhomefilesystem.db.manager.PcSysManage;
import com.uhome.file.uhomefilesystem.db.param.OprateParam;
import com.uhome.file.uhomefilesystem.db.param.SysRoleParam;
import com.uhome.file.uhomefilesystem.db.param.SysUserParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/pc")
public class PcSysController extends AbstractController {

    @Autowired
    private PcSysManage sysManage;


    @GetMapping("getDA")
    public ResultCode getDA() {
        return sysManage.getDA();
    }

    @GetMapping("addCer")
    public ResultCode addCer(String name,String type) {
        return sysManage.addCer(name,type);
    }

    @GetMapping("deleteCer")
    public ResultCode deleteCer(Long id) {
        return sysManage.deleteCer(id);
    }
    @GetMapping("selectRoleList")
    public ResultCode selectRoleList() {
        return sysManage.selectRoleList();
    }
    @GetMapping("selectRoleListWithMenu")
    public ResultCode selectRoleListWithMenu() {
        return sysManage.selectRoleListWithMenu();
    }
    @GetMapping("selectMenuList")
    public ResultCode selectMenuList() {
        return sysManage.selectMenuList();
    }

    @GetMapping("deleteRole")
    public ResultCode deleteRole(String id) {
        if(StringUtils.isBlank(id)){
            return ResultCode.error("请勾选数据");
        }
        String [] arrs = id.split(",");
        List<Long> idList = new ArrayList<>();
        for (int i = 0; i < arrs.length; i++) {
            idList.add(Long.parseLong(arrs[i]));
        }
        return sysManage.deleteRole(idList);
    }

    @GetMapping("saveRole")
    public ResultCode saveRole(SysRoleParam param) {
        if(StringUtils.isEmpty(param.getMenuIdStr())){
            return ResultCode.error();
        }
        List<Long> midP = new ArrayList<>();
        String [] list =  param.getMenuIdStr().split(",");
        for (int i = 0; i <list.length ; i++) {
            midP.add(Long.parseLong(list[i]));
        }
        SysRoleParam sysRoleParam = new SysRoleParam();
        sysRoleParam.setName(param.getName());
        sysRoleParam.setMenuIds(midP);
        sysRoleParam.setCity(param.getCity());
        sysRoleParam.setId(param.getId());
        return sysManage.saveRole(sysRoleParam);
    }


    @GetMapping("selectUser")
    public ResultCode selectUser(SysUserParam sysUserParam,PageInfo pageable) {
        return sysManage.selectUser(sysUserParam,pageable);
    }
    @GetMapping("selectUser2")
    public ResultCode selectUser2(SysUserParam sysUserParam, PageInfo pageable) {
        return sysManage.selectUser2(sysUserParam,pageable);
    }
    @GetMapping("deleteUser")
    public ResultCode deleteUser(String ids) {
        List<Long> midP = new ArrayList<>();
        String [] list =  ids.split(",");
        for (int i = 0; i <list.length ; i++) {
            midP.add(Long.parseLong(list[i]));
        }

        return sysManage.deleteUser(midP);
    }

    @GetMapping("saveUser")
    public ResultCode saveUser(Long roleId,String userIds) {
        List<String> midP = new ArrayList<>();
        String [] list =  userIds.split(",");
        for (int i = 0; i <list.length ; i++) {
            midP.add(list[i]);
        }
        SysUserParam sysUserParam = new SysUserParam();
        sysUserParam.setRoleId(roleId);
        sysUserParam.setUserIds(midP);
        return sysManage.saveUser(sysUserParam);
    }

    @GetMapping("query_certificate")
    public ResultCode queryCertificate(String certificateType){
        return sysManage.queryCertificate(certificateType);
    }

    @GetMapping("saveLoginLog")
    public ResultCode saveLoginLog(HttpServletRequest request) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }
            FsLoginLog fsLoginLog = new FsLoginLog();
            fsLoginLog.setUserId(user.getId());
            fsLoginLog.setCity(user.getCity());
            fsLoginLog.setDept(user.getDeptName());
            fsLoginLog.setName(user.getName());
            ResultCode result = sysManage.saveLoginLog(fsLoginLog);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

    @GetMapping("saveOprateLog")
    public ResultCode saveOprateLog(OprateParam oprateParam,HttpServletRequest request) {
        try {
            SysUser user = getUser(request);
            if (user == null || user.getId() == null) {
                return ResultCode.error("请重新登录");
            }
            oprateParam.setUserId(user.getId());
            oprateParam.setUserName(user.getName());
            ResultCode result = sysManage.saveOprateLog(oprateParam);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultCode.error("后台异常");
        }
    }

}
