package com.uhome.file.uhomefilesystem.db.service.impl;

import com.uhome.file.uhomefilesystem.db.dao.FsProductDetailMapper;
import com.uhome.file.uhomefilesystem.db.service.IFsProductDetilService;
import com.uhome.file.uhomefilesystem.db.vo.HProductDetailVo;
import com.uhome.file.uhomefilesystem.db.vo.ProductDetailDingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author zbb
 * @since 2021-06-19
 */
@Service
public class FsProductDetailServiceImpl implements IFsProductDetilService {

    @Autowired
    FsProductDetailMapper fsProductDetailMapper;


    @Override
    public List<HProductDetailVo> queryProductDetailData(Long id) {
        return fsProductDetailMapper.queryProductDetailData(id);
    }

    @Override
    public List<ProductDetailDingVo> getListByRemindTime() {
        return fsProductDetailMapper.getListByRemindTime();
    }
}
