package com.uhome.file.uhomefilesystem.db.param;

/**
 * 
 *
 * @author zb
 * @date 2021/07/04
 */
public class FsProductListParam {
    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品编码
     */
    private String code;

    /**
     * 编辑人
     */
    private String editor;

    /**
     * 录入状态  0.未录入 1.部分录入 2、已录入
     */
    private String recordType;

    private String productCate1;

    private String productCate2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getProductCate1() {
        return productCate1;
    }

    public void setProductCate1(String productCate1) {
        this.productCate1 = productCate1;
    }

    public String getProductCate2() {
        return productCate2;
    }

    public void setProductCate2(String productCate2) {
        this.productCate2 = productCate2;
    }
}