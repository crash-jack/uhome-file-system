package com.uhome.file.uhomefilesystem.db.service;


import cn.hutool.json.JSONArray;
import com.uhome.file.uhomefilesystem.db.entity.FsProductList;
import com.uhome.file.uhomefilesystem.db.param.FsProductListParam;
import com.uhome.file.uhomefilesystem.db.vo.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author zbb
 * @since 2020-07-14
 */
public interface IFsProductListService {

    List<HProductVo> queryProductData(String queryStr);

    List<FsProductList> queryProductList(FsProductListParam productListParam);

    List<PProductDetailVo> queryProductListDetail(Long id);

    /**
     * 查询商品种类
     */
    Map<String, Set<String>> getCurrUserProductType();

    JSONArray getCurrUserProductTypeTree();

    /**
     * 获取商品数量信息
     */
    TotalDataVo getTotalData();

    /**
     * 获取商品完成情况
     *
     * @param type 商品种类
     */
    TotalBaseVo getTypeEnRate(Integer type, String name);

    /**
     * 获取商品完成情况
     *
     * @param type 商品种类
     */
    TotalEnRateVo getTypeRate(Integer type, String name);

    /**
     * 导出商品数据
     *
     * @param type     type
     * @param response response
     */
    void export(int type, HttpServletResponse response);

    ProductDetailDingVo getDingById(String id);

    int updateById(FsProductList fsProductList);

    int getRecordState(Long id,Integer type);
}
