package com.uhome.file.uhomefilesystem.db.vo;

import lombok.Data;

import java.util.Date;

@Data
public class StoreDetailDingVo {

    private String ddUserId;

    private String city;

    private String address;

    private String storeName;

    private String managerName;

    private String detailName;

    private Date remindTime;
}
