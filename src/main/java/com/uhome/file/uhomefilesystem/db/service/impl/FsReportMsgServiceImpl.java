package com.uhome.file.uhomefilesystem.db.service.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.uhome.file.uhomefilesystem.config.datasources.DataSourceNames;
import com.uhome.file.uhomefilesystem.config.datasources.annotation.DataSource;
import com.uhome.file.uhomefilesystem.config.dingmsg.service.DingMsgService;
import com.uhome.file.uhomefilesystem.db.dao.FsReportMsgMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsReportMsg;
import com.uhome.file.uhomefilesystem.db.service.IFsProductListService;
import com.uhome.file.uhomefilesystem.db.service.IFsReportMsgService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreListService;
import com.uhome.file.uhomefilesystem.db.vo.ProductDetailDingVo;
import com.uhome.file.uhomefilesystem.db.vo.StoreDetailDingVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@RequiredArgsConstructor
@Service
public class FsReportMsgServiceImpl implements IFsReportMsgService {

    private final IFsStoreListService iFsStoreListService;
    private final IFsProductListService iFsProductListService;
    private final FsReportMsgMapper fsReportMsgMapper;
    private final DingMsgService dingMsgService;

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public Integer reportMsg(Map<String, Object> map) {
        Integer type = MapUtil.getInt(map, "type");
        String id = MapUtil.getStr(map, "id");
        if (StrUtil.isNotEmpty(id)) {

            FsReportMsg msg = new FsReportMsg();
            msg.setType(type);
            msg.setLinkId(Long.parseLong(id));
            msg.setContext(MapUtil.getStr(map, "context"));

            //保存数据
            Integer insert = fsReportMsgMapper.insert(msg);
            if (insert > 0) {
                if (type == 0) {
                    //店铺上报
                    StoreDetailDingVo store = iFsStoreListService.getDingById(id);
                    if (store != null) {
                        //发送钉钉消息
                        dingMsgService.sendStoreErrContext(store);
                    }
                } else if (type == 1) {
                    //商品上报
                    ProductDetailDingVo product = iFsProductListService.getDingById(id);
                    if (product != null) {
                        //发送钉钉消息
                        dingMsgService.sendProductErrContext(product);
                    }
                }
            }
            return insert;
        }
        return 0;
    }
}
