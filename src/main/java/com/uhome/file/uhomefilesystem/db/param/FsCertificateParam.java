package com.uhome.file.uhomefilesystem.db.param;

/**
 * 
 *
 * @author zb
 * @date 2021/07/04
 */
public class FsCertificateParam {
    /**
     * 
     */
    private Long id;

    /**
     * 证件名称
     */
    private String name;

    /**
     * 证件排序
     */
    private Integer sort;

    /**
     * 状态 0.显示 1.不显示
     */
    private String state;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column fs_certificate.id
     *
     * @return the value of fs_certificate.id
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column fs_certificate.id
     *
     * @param id the value for fs_certificate.id
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column fs_certificate.name
     *
     * @return the value of fs_certificate.name
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column fs_certificate.name
     *
     * @param name the value for fs_certificate.name
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column fs_certificate.sort
     *
     * @return the value of fs_certificate.sort
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column fs_certificate.sort
     *
     * @param sort the value for fs_certificate.sort
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column fs_certificate.state
     *
     * @return the value of fs_certificate.state
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    public String getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column fs_certificate.state
     *
     * @param state the value for fs_certificate.state
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    public void setState(String state) {
        this.state = state;
    }
}