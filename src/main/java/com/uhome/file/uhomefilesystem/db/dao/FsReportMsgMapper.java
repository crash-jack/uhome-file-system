package com.uhome.file.uhomefilesystem.db.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsReportMsg;
import org.springframework.stereotype.Repository;

@Repository
public interface FsReportMsgMapper extends BaseMapper<FsReportMsg> {
}