package com.uhome.file.uhomefilesystem.db.vo;

import lombok.Data;

import java.util.Set;

@Data
public class StoreCityAreaVo {

    private Set<String> city;

    private Set<String> area;
}
