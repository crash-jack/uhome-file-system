package com.uhome.file.uhomefilesystem.db.param;

/**
 * 
 *
 * @author zb
 * @date 2021/07/04
 */
public class FsProductDetailParam {

    /**
     * fs_product_detail表主键ID
     */
    private Long id;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 
     */
    private Long certificateId;

    /**
     * 证书地址
     */
    private String img;

    /**
     * pdf转成png，供前端显示使用
     */
    private String pngImg;

    /**
     * 证件名称
     */
    private String name;

    /**
     * 提醒日期
     */
    private String remindTime;

    /**
     * 统一社会信用代码
     */
    private String code;

    /**
     * 修改人
     */
    private Long updator;

    public Long getUpdator() {
        return updator;
    }

    public void setUpdator(Long updator) {
        this.updator = updator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemindTime() {
        return remindTime;
    }

    public void setRemindTime(String remindTime) {
        this.remindTime = remindTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPngImg() {
        return pngImg;
    }

    public void setPngImg(String pngImg) {
        this.pngImg = pngImg;
    }
}