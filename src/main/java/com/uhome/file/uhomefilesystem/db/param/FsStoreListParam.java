package com.uhome.file.uhomefilesystem.db.param;

/**
 * 
 *
 * @author zb
 * @date 2021/07/04
 */
public class FsStoreListParam {

    /**
     * 城市
     */
    private String city;


    private String address;
    /**
     * 店号
     */
    private String code;

    /**
     * 区域
     */
    private String area;

    /**
     * 店名
     */
    private String name;


    /**
     * 店长姓名
     */
    private String managerName;

    /**
     * 店铺类型 0.个人 1 公司
     */
    private String storeType;

    /**
     * 门店状态 0.开店 1.关店
     */
    private String storeState;

    /**
     * 录入状态 0.未录入 1.已录入
     */
    private String recordType;

    /**
     * 店长id
     */
    private Long managerId;

    /**
     * 编辑人
     */
    private String editor;

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public String getStoreState() {
        return storeState;
    }

    public void setStoreState(String storeState) {
        this.storeState = storeState;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }
}