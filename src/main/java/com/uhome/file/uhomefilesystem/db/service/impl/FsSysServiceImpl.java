package com.uhome.file.uhomefilesystem.db.service.impl;

import com.uhome.file.uhomefilesystem.config.datasources.DataSourceNames;
import com.uhome.file.uhomefilesystem.config.datasources.annotation.DataSource;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.dao.*;
import com.uhome.file.uhomefilesystem.db.entity.*;
import com.uhome.file.uhomefilesystem.db.param.OprateParam;
import com.uhome.file.uhomefilesystem.db.param.SysRoleParam;
import com.uhome.file.uhomefilesystem.db.param.SysUserParam;
import com.uhome.file.uhomefilesystem.db.service.IFsSysService;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author zbb
 * @since 2021-06-19
 */
@Service
public class FsSysServiceImpl implements IFsSysService {

    @Autowired
    SysUserMapper sysUserMapper;

    @Autowired
    SysRoleMapper sysRoleMapper;

    @Autowired
    FsCertificateMapper fsCertificateMapper;

    @Autowired
    SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    SysRoleMenuMapper sysRoleMenuMapper;

    @Autowired
    SysMenuMapper sysMenuMapper;

    @Autowired
    FsLoginLogMapper fsLoginLogMapper;

    @Autowired
    FsOprateLogMapper fsOprateLogMapper;
//
//    @Autowired
//    SysRoleMapper sysRoleMapper;
//    @Autowired
//    SysRoleMapper sysRoleMapper;


    @Override
    public List<FsCertificate> getDa() {
        return fsCertificateMapper.getDa();
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void addCer(String name,String type) {
        FsCertificate fsCertificate = new FsCertificate();
        fsCertificate.setName(name);
        fsCertificate.setState("0");
        fsCertificate.setType(type);
        fsCertificateMapper.insertSelective(fsCertificate);
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void deleteCer(Long id) {
        FsCertificate fsCertificate = new FsCertificate();
        fsCertificate.setId(id);
        fsCertificate.setState("1");
        fsCertificateMapper.updateByPrimaryKeySelective(fsCertificate);
    }

    @Override
    public List<SysRole> selectRole() {
        return sysRoleMapper.selectRole();
    }

    @Override
    public List<SysRoleMenu> selectRoleMenu(Long id) {
        return sysRoleMapper.selectRoleMenu(id);
    }

    @Override
    public List<SysUser> selectUserSelect(SysUserParam sysUserParam) {
        return sysUserMapper.selectUserSelect(sysUserParam);
    }

    @Override
    public List<SysUser> selectUser(SysUserParam sysUserParam) {
        return sysUserMapper.selectUser(sysUserParam);
    }

    @Override
    @DataSource(name = DataSourceNames.THIRD)
    public List<DimEmployeeInfo> selectUser2(SysUserParam sysUserParam) {
        return sysUserMapper.selectUser2(sysUserParam);
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void deleteUser(Long id) {
        SysUser sysUserParam = new SysUser();
        sysUserParam.setId(id);
        sysUserParam.setState("1");
        sysUserMapper.updateByPrimaryKeySelective(sysUserParam);
    }

    @Override
    @DataSource(name = DataSourceNames.THIRD)
    public List<DimEmployeeInfo> selectUser2ByIds(List<String> userIds) {
        return  sysUserMapper.selectUser2ByIds(userIds);
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void insertUser(SysUser sysUser) {
        sysUserMapper.insertSelective(sysUser);
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void updateRole(Long userId, Long roleId) {
        sysUserRoleMapper.deleteByUserId(userId);
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setUserId(userId);
        sysUserRole.setRoleId(roleId);
        sysUserRoleMapper.insertSelective(sysUserRole);
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void saveRole(SysRoleParam sysRoleParam) {
        if(sysRoleParam.getMenuIds()!=null && sysRoleParam.getMenuIds().size()>0){
            SysRole sysRole = new SysRole();
            if(sysRoleParam.getId()!=null){
                sysRole.setState("1");
                sysRole.setName(sysRoleParam.getName());
                sysRole.setCity(sysRoleParam.getCity());
                sysRole.setId(sysRoleParam.getId());
                sysRoleMapper.updateByPrimaryKeySelective(sysRole);
            }else{
                sysRole.setState("1");
                sysRole.setCity(sysRoleParam.getCity());
                sysRole.setName(sysRoleParam.getName());
                sysRoleMapper.insertSelective(sysRole);
            }
            sysRoleMenuMapper.deleteByRoleId(sysRole.getId());
            for (int i = 0; i < sysRoleParam.getMenuIds().size(); i++) {
                SysRoleMenu sysRoleMenu = new SysRoleMenu();
                sysRoleMenu.setMenuId(sysRoleParam.getMenuIds().get(i));
                sysRoleMenu.setRoleId(sysRole.getId());
                sysRoleMenuMapper.insertSelective(sysRoleMenu);
            }
        }
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void deleteRole(Long id) {
        SysRole sysRole = new SysRole();
        sysRole.setId(id);
        sysRole.setState("0");
        sysRoleMapper.updateByPrimaryKeySelective(sysRole);

        sysRoleMapper.deleteUserRole(id);
    }

    @Override
    public List<SysMenu> selectMenuList() {
        return sysMenuMapper.selectMenuList();
    }

    @Override
    public String getTempId(Long id) {
        return sysUserMapper.getTempId(id);
    }

    @Override
    public List<SysMenu> selectUserMenu(Long id,Integer type) {
        return sysUserMapper.selectUserMenu(id,type);
    }

    @Override
    public List<SysRole> selectRoleByName(String name) {
        return sysRoleMapper.selectRoleByName(name);
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public ResultCode saveLoginLog(FsLoginLog fsLoginLog) {
          fsLoginLogMapper.insertSelective(fsLoginLog);
          return ResultCode.ok();
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public ResultCode saveOprateLog(OprateParam oprateParam) {
        FsOprateLog fsOprateLog = new FsOprateLog();
        BeanUtils.copyProperties(oprateParam, fsOprateLog);
        fsOprateLogMapper.insertSelective(fsOprateLog);
        return ResultCode.ok();
    }

    @Override
    public List<SysRole> getUserCity(Long userId) {

        List<SysRole> sysRoleList =  sysRoleMapper.getUserCity(userId);
        return sysRoleList;
    }
}
