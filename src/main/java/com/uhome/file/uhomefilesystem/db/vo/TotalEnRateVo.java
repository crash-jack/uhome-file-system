package com.uhome.file.uhomefilesystem.db.vo;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class TotalEnRateVo {
    private Set<String> type;
    /**
     * 录入完成
     */
    private List<Integer> totalRates;

    /**
     * 未录入总数
     */
    private List<Integer> notTotalRates;

    /**
     * 部分录入总数
     */
    private List<Integer> partRates;
}
