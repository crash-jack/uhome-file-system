package com.uhome.file.uhomefilesystem.db.param;

import com.uhome.file.uhomefilesystem.db.vo.PStoreDetailVo;

import java.util.ArrayList;
import java.util.List;

public class MidStoreParam {

    private Long id;

    private String name;

    private List<PStoreDetailVo> urlList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PStoreDetailVo> getUrlList() {
        return urlList;
    }

    public void setUrlList(List<PStoreDetailVo> urlList) {
        this.urlList = urlList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
