package com.uhome.file.uhomefilesystem.db.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.uhome.file.uhomefilesystem.config.datasources.DataSourceNames;
import com.uhome.file.uhomefilesystem.config.datasources.annotation.DataSource;
import com.uhome.file.uhomefilesystem.config.utils.ExportDataUtils;
import com.uhome.file.uhomefilesystem.db.dao.FsProductQueryMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsProductQuery;
import com.uhome.file.uhomefilesystem.db.service.IFsProductQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author bowen.li
 * @since 2021-08-24
 */
@RequiredArgsConstructor
@Service
public class FsProductQueryServiceImpl implements IFsProductQueryService {
    private final FsProductQueryMapper fsProductQueryMapper;

    @Override
    public List<Map<String, Object>> getCountTotal(Integer type, String name) {
        return fsProductQueryMapper.getCountTotalByType(type, name);
    }

    @Override
    public void export(Integer type, HttpServletResponse response) {
        List<Map<String, Object>> list = fsProductQueryMapper.getExportDate(ExportDataUtils.exportTypeToData(type));
        ArrayList<Map<String, Object>> courseDtoList = new ArrayList<>();
        for (Map<String, Object> li : list) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("商品名称", MapUtil.getStr(li, "name"));
            map.put("商品编码", MapUtil.getStr(li, "code"));
            map.put("商品规格", MapUtil.getStr(li, "norm"));
            map.put("商品品类", MapUtil.getStr(li, "product_type"));
            map.put("商品状态", ExportDataUtils.getProductState(MapUtil.getStr(li, "code")));
            map.put("录入状态", ExportDataUtils.getRecordType(MapUtil.getStr(li, "record_type")));
            courseDtoList.add(map);
        }
        ServletOutputStream ouPutStream = null;
        //导出成excel
        try (ExcelWriter writer = ExcelUtil.getWriter()) {
            ExportDataUtils.setWriterResponse(response, DateUtil.now());
            if (CollUtil.isNotEmpty(courseDtoList)) {
                writer.write(courseDtoList, true);
            } else {
                List<String> head = new ArrayList<>();
                head.add("商品名称");
                head.add("商品编码");
                head.add("商品规格");
                head.add("商品品类");
                head.add("商品状态");
                head.add("录入状态");
                writer.writeHeadRow(head);
            }
            ouPutStream = response.getOutputStream();
            writer.flush(ouPutStream, true);
        } catch (IOException e) {
            throw new RuntimeException("导出失败");
        } finally {
            IoUtil.close(ouPutStream);
        }
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void productLog(Integer type, Long productId, Long certificateId) {
        if (productId != null) {
            FsProductQuery entity = new FsProductQuery();
            entity.setProductId(productId);
            entity.setType(type);
            entity.setCreateTime(LocalDateTime.now());
            entity.setCertificateId(certificateId);
            fsProductQueryMapper.insert(entity);
        }
    }
}
