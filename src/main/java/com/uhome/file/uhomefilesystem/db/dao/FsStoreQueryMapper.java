package com.uhome.file.uhomefilesystem.db.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsStoreQuery;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author bowen.li
 * @since 2021-08-24
 */
@Repository
public interface FsStoreQueryMapper extends BaseMapper<FsStoreQuery> {

    List<Map<String, Object>> getCountTotalByCertType(@Param("cityList") List<String> cityList);

    List<Map<String, Object>> getCountTotalByType(@Param("cityList") List<String> cityList);

    List<Map<String, Object>> getExportDate(@Param("city") String city, @Param("time") String time);
}
