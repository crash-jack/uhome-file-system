package com.uhome.file.uhomefilesystem.db.vo;

import lombok.Data;

import java.util.Date;

@Data
public class PStoreDetailVo {
    private Long id;

    /**
     * 店铺ID
     */
    private Long storeId;
    /**
     * 店铺类型 0.个人 1 公司
     */
    private String storeType;

    /**
     *
     */
    private Long productId;

    /**
     *
     */
    private Long certificateId;

    private String certificateName;

    /**
     * 统一社会信用代码
     */
    private String code;

    /**
     * 证书地址
     */
    private String img;

    /**
     * 证书地址
     */
    private String pngImg;

    /**
     * 证件名称
     */
    private String name;

    /**
     * 提醒日期
     */
    private Date remindTime;

    /**
     *
     */
    private Date createTime;

    /**
     *
     */
    private Long creator;

    /**
     *
     */
    private Date updateTime;

    /**
     *
     */
    private Long updator;

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRemindTime() {
        return remindTime;
    }

    public void setRemindTime(Date remindTime) {
        this.remindTime = remindTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdator() {
        return updator;
    }

    public void setUpdator(Long updator) {
        this.updator = updator;
    }

    public String getPngImg() {
        return pngImg;
    }

    public void setPngImg(String pngImg) {
        this.pngImg = pngImg;
    }
}
