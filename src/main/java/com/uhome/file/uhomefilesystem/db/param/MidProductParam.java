package com.uhome.file.uhomefilesystem.db.param;

import com.uhome.file.uhomefilesystem.db.vo.PProductDetailVo;

import java.util.ArrayList;
import java.util.List;

public class MidProductParam {

    private Long id;
    private String name;

    private List<PProductDetailVo> urlList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PProductDetailVo> getUrlList() {
        return urlList;
    }

    public void setUrlList(List<PProductDetailVo> urlList) {
        this.urlList = urlList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
