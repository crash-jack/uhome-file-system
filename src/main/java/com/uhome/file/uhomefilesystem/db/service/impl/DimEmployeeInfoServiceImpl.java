package com.uhome.file.uhomefilesystem.db.service.impl;

import com.uhome.file.uhomefilesystem.config.datasources.DataSourceNames;
import com.uhome.file.uhomefilesystem.config.datasources.annotation.DataSource;
import com.uhome.file.uhomefilesystem.db.dao.DimEmployeeInfoMapper;
import com.uhome.file.uhomefilesystem.db.dao.FsProductDetailMapper;
import com.uhome.file.uhomefilesystem.db.entity.DimEmployeeInfo;
import com.uhome.file.uhomefilesystem.db.param.DimEmployeeParam;
import com.uhome.file.uhomefilesystem.db.service.IDimEmployeeInfoService;
import com.uhome.file.uhomefilesystem.db.service.IFsProductDetilService;
import com.uhome.file.uhomefilesystem.db.vo.HProductDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author zbb
 * @since 2021-06-19
 */
@Service
public class DimEmployeeInfoServiceImpl implements IDimEmployeeInfoService {

    @Autowired
    DimEmployeeInfoMapper dimEmployeeInfoMapper;


    @Override
    @DataSource(name = DataSourceNames.THIRD)
    public List<DimEmployeeInfo> queryUserByFiled(DimEmployeeParam dimEmployeeParam) {
        return dimEmployeeInfoMapper.queryUserByFiled(dimEmployeeParam);
    }

}
