package com.uhome.file.uhomefilesystem.db.service;

import com.uhome.file.uhomefilesystem.db.entity.SysUser;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author bowen.li
 * @since 2021-08-24
 */
public interface IFsStoreQueryService {
    List<Map<String, Object>> getCountTotalByCertType(SysUser user, String city);

    /**
     * 查询店铺查询次数
     */
    List<Map<String, Object>> getCountTotal(SysUser user, String city);

    void export(SysUser user, int type, HttpServletResponse response);

    void storeLog(Integer type, Long storeId, Long certificateId);
}
