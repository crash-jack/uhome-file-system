package com.uhome.file.uhomefilesystem.db.entity;

import lombok.Data;

import java.util.Date;

@Data
public class FsReportMsg {

    private Long id;

    private Integer type;

    private Long linkId;

    private String context;

    private Date createTime;
}
