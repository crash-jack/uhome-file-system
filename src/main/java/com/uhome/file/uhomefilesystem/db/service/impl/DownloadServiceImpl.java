package com.uhome.file.uhomefilesystem.db.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ZipUtil;
import com.uhome.file.uhomefilesystem.common.enums.CertificateDeptEnum;
import com.uhome.file.uhomefilesystem.db.dao.FsProductDetailMapper;
import com.uhome.file.uhomefilesystem.db.dao.FsStoreDetailMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsProductDetail;
import com.uhome.file.uhomefilesystem.db.entity.FsStoreDetail;
import com.uhome.file.uhomefilesystem.db.param.DownloadParam;
import com.uhome.file.uhomefilesystem.db.service.IDownloadService;
import com.uhome.file.uhomefilesystem.db.vo.DProductDetailVO;
import com.uhome.file.uhomefilesystem.db.vo.DStoreDetailVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Objects;

@Service
public class DownloadServiceImpl implements IDownloadService {
//    private static final String DOWN_TEMP_PATH = "C:" + File.separator + "DOWNLOAD" + File.separator + "TEMP";
    @Autowired
    private FsProductDetailMapper fsProductDetailMapper;
    @Autowired
    private FsStoreDetailMapper fsStoreDetailMapper;

    @Value("${download.downTempPath}")
    private String downTempPath;

    @Value("${upload.imagesAddress}")
    private String imagesAddress;

    @Override
    public void file(DownloadParam downloadParam, HttpServletResponse response) throws Exception {
        String imgPath = null;
        if (CertificateDeptEnum.SHANG_PIN_BU.getCode().equals(downloadParam.getType())) {
            FsProductDetail fsProductDetail = fsProductDetailMapper.selectByPrimaryKey(downloadParam.getId());
            imgPath = fsProductDetail.getImg();
        } else if (CertificateDeptEnum.ZHENG_WU_BU.getCode().equals(downloadParam.getType())) {
            FsStoreDetail fsStoreDetail = fsStoreDetailMapper.selectByPrimaryKey(downloadParam.getId());
            imgPath = fsStoreDetail.getImg();
        }

        download(imagesAddress+imgPath, response);
    }

    @Override
    public void batchProduct(String ids, HttpServletResponse response) throws Exception {
        List<DProductDetailVO> productDetailList = fsProductDetailMapper.queryProductDetailList(ids);
        String filePath = downTempPath + File.separator + IdUtil.simpleUUID() + File.separator; //临时目录
        File tempFile = new File(filePath);
        if (!tempFile.exists()) {
            tempFile.mkdirs();
        }
        for (DProductDetailVO productDetail : productDetailList) {
            String productName = productDetail.getProductName();
            String certificateName = productDetail.getCertificateName();
            String img = productDetail.getImg();
            File imgFile = new File(imagesAddress + img);
            if (imgFile.exists()) {
                String fileName = imgFile.getName();
                String descPath = filePath + productName + File.separator + certificateName + File.separator + fileName;
                try {
                    FileUtil.copy(imgFile, new File(descPath), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (!productDetailList.isEmpty()) {
            File zipFile = ZipUtil.zip(filePath);

            download(zipFile.getPath(), response);
        } else {
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write("没有上传证书");
        }


    }

    @Override
    public void batchStore(String ids, HttpServletResponse response) throws Exception {
        List<DStoreDetailVO> storeDetailList = fsStoreDetailMapper.queryStoreDetailList(ids);
        String filePath = downTempPath + File.separator + IdUtil.simpleUUID() + File.separator;
        File tempFile = new File(filePath);
        if (!tempFile.exists()) {
            tempFile.mkdirs();
        }
        for (DStoreDetailVO storeDetailVO : storeDetailList) {
            String storeName = storeDetailVO.getStoreName();
            String certificateName = storeDetailVO.getCertificateName();
            String img = storeDetailVO.getImg();
            File imgFile = new File(imagesAddress+img);
            if (imgFile.exists()) {
                String fileName = imgFile.getName();
                String descPath = filePath + storeName + File.separator + certificateName + File.separator + fileName;
                try {
                    FileUtil.copy(imgFile, new File(descPath), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        if (!storeDetailList.isEmpty()) {
            File zipFile = ZipUtil.zip(filePath);

            download(zipFile.getPath(), response);
        } else {
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write("没有上传证书");
        }

    }


    private void download(String filePath, HttpServletResponse response) throws Exception {
        if (Objects.nonNull(filePath)) {
            File file = new File(filePath);
            if (file.exists()) {
                String fileName = file.getName();

                // 设置下载格式
                response.setContentType("application/octet-stream;charset=UTF-8");
                response.addHeader("Content-Disposition", "attachment;fileName=" + new String(fileName.getBytes("UTF-8"), "ISO8859-1"));

                try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
                     BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream())
                ) {
                    byte[] buffer = new byte[1024];
                    int read;
                    while ((read = bis.read(buffer)) != -1) {
                        bos.write(buffer, 0, read);
                    }
                }
            } else {
                throw new RuntimeException("文件不存在");
            }
        } else {
            throw new RuntimeException("文件不存在");
        }
    }
}
