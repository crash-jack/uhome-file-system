package com.uhome.file.uhomefilesystem.db.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author bowen.li
 * @since 2021-08-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class FsStoreQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 门店id
     */
    private Long storeId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    private Integer type;

    private Long certificateId;
}
