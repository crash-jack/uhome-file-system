package com.uhome.file.uhomefilesystem.db.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.uhome.file.uhomefilesystem.config.datasources.DataSourceNames;
import com.uhome.file.uhomefilesystem.config.datasources.annotation.DataSource;
import com.uhome.file.uhomefilesystem.config.utils.ExportDataUtils;
import com.uhome.file.uhomefilesystem.db.dao.FsStoreQueryMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsStoreQuery;
import com.uhome.file.uhomefilesystem.db.entity.SysUser;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author bowen.li
 * @since 2021-08-24
 */
@RequiredArgsConstructor
@Service
public class FsStoreQueryServiceImpl implements IFsStoreQueryService {

    private final FsStoreQueryMapper fsStoreQueryMapper;

    @Override
    public List<Map<String, Object>> getCountTotalByCertType(SysUser user, String city) {
        String finalCity = setCity(city);
        List<String> cityList = new ArrayList<>();
        if (StrUtil.isNotEmpty(user.getCity())) {
            String [] arrs = user.getCity().split(",");
            for (int i = 0; i < arrs.length; i++) {
                cityList.add(arrs[i]);
            }
        }

        if (StrUtil.isNotEmpty(finalCity)) {
            cityList.add(finalCity);
        }
        return fsStoreQueryMapper.getCountTotalByCertType(cityList);
    }

    @Override
    public List<Map<String, Object>> getCountTotal(SysUser user, String city) {
        String finalCity = setCity(city);
        List<String> cityList = new ArrayList<>();
        if (StrUtil.isNotEmpty(user.getCity())) {
//            cityList.add(user.getCity());
            String [] arrs = user.getCity().split(",");
            for (int i = 0; i < arrs.length; i++) {
                cityList.add(arrs[i]);
            }
        }

        if (StrUtil.isNotEmpty(finalCity)) {
            cityList.add(finalCity);
        }
        return fsStoreQueryMapper.getCountTotalByType(cityList);
    }

    @Override
    public void export(SysUser user, int type, HttpServletResponse response) {
        List<Map<String, Object>> list = fsStoreQueryMapper.getExportDate(user.getCity(), ExportDataUtils.exportTypeToData(type));
        ArrayList<Map<String, Object>> courseDtoList = new ArrayList<>();
        for (Map<String, Object> li : list) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("城市", MapUtil.getStr(li, "city"));
            map.put("区域", MapUtil.getStr(li, "area"));
            map.put("地址", MapUtil.getStr(li, "address"));
            map.put("店名", MapUtil.getStr(li, "name"));
            map.put("店号", MapUtil.getStr(li, "code"));
            map.put("店长姓名", MapUtil.getStr(li, "manager_name"));
            map.put("店铺类型", ExportDataUtils.getStoreType(MapUtil.getStr(li, "store_type")));
            map.put("店铺状态", ExportDataUtils.getStoreState(MapUtil.getStr(li, "store_state")));
            map.put("录入状态", ExportDataUtils.getRecordType(MapUtil.getStr(li, "record_type")));
            courseDtoList.add(map);
        }
        ServletOutputStream ouPutStream = null;
        //导出成excel
        try (ExcelWriter writer = ExcelUtil.getWriter()) {
            ExportDataUtils.setWriterResponse(response, DateUtil.now());
            if (CollUtil.isNotEmpty(courseDtoList)) {
                writer.write(courseDtoList, true);
            } else {
                List<String> head = new ArrayList<>();
                head.add("城市");
                head.add("区域");
                head.add("地址");
                head.add("店名");
                head.add("店号");
                head.add("店长姓名");
                head.add("店铺类型");
                head.add("店铺状态");
                head.add("录入状态");
                writer.writeHeadRow(head);
            }
            ouPutStream = response.getOutputStream();
            writer.flush(ouPutStream, true);
        } catch (IOException e) {
            throw new RuntimeException("导出失败");
        } finally {
            IoUtil.close(ouPutStream);
        }
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void storeLog(Integer type, Long storeId, Long certificateId) {
        if (storeId != null) {
            FsStoreQuery entity = new FsStoreQuery();
            entity.setStoreId(storeId);
            entity.setCreateTime(LocalDateTime.now());
            entity.setType(type);
            entity.setCertificateId(certificateId);
            fsStoreQueryMapper.insert(entity);
        }
    }

    private String setCity(String city) {
        if (StrUtil.isNotEmpty(city) && city.equals("0")) {
            return null;
        }
        return city;
    }
}
