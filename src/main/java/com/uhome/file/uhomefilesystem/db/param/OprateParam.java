package com.uhome.file.uhomefilesystem.db.param;

import lombok.Data;

@Data
public class OprateParam {
    /**
     * 操作模块
     */
    private String model;

    /**
     * 操作
     */
    private String oprate;

    /**
     * 操作人
     */
    private Long userId;

    /**
     * 操作人姓名
     */
    private String userName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOprate() {
        return oprate;
    }

    public void setOprate(String oprate) {
        this.oprate = oprate;
    }
}
