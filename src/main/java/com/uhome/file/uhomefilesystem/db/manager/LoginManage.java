package com.uhome.file.uhomefilesystem.db.manager;
import cn.hutool.json.JSONUtil;
import com.uhome.file.uhomefilesystem.config.constant.TokenPre;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.config.utils.TokenCreate;
import com.uhome.file.uhomefilesystem.db.entity.DimEmployeeInfo;
import com.uhome.file.uhomefilesystem.db.entity.SysMenu;
import com.uhome.file.uhomefilesystem.db.entity.SysRole;
import com.uhome.file.uhomefilesystem.db.entity.SysUser;
import com.uhome.file.uhomefilesystem.db.param.DimEmployeeParam;
import com.uhome.file.uhomefilesystem.db.param.SysUserParam;
import com.uhome.file.uhomefilesystem.db.service.IDimEmployeeInfoService;
import com.uhome.file.uhomefilesystem.db.service.IFsSysService;
import com.uhome.file.uhomefilesystem.db.service.IRedisService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class LoginManage {

    @Autowired
    IDimEmployeeInfoService dimEmployeeInfoService;

    @Autowired
    IFsSysService fsSysService;

    @Autowired
    IRedisService redisService;

    public ResultCode qrLogin(String phone) {
        Map<String,Object> map = new HashMap<>();
        DimEmployeeParam dimEmployeeParam = new DimEmployeeParam();
        dimEmployeeParam.setPhone(phone);
        List<DimEmployeeInfo> list = dimEmployeeInfoService.queryUserByFiled(dimEmployeeParam);
        String workPlace = list.get(0).getWorkPlace();
//        String city = null;
//        if(StringUtils.isNotBlank(workPlace)){
//            if(workPlace.length()== 4){
//                city = workPlace.substring(2,3);
//            }
//            if(workPlace.length()== 2){
//                city = workPlace;
//            }
//        }
        //账号不存在
        if (list == null||list.size()==0) {
            return ResultCode.error("用户钉钉不在有家系统中");
        }
        SysUser user ;
        SysUserParam sysUserParam = new SysUserParam();
        sysUserParam.setPhone(phone);
        List<SysUser> userList = fsSysService.selectUserSelect(sysUserParam);
        //用户没登录 在表里注册
        if(userList==null||userList.size()==0){
            return ResultCode.error("用户不在系统中，请联系管理人员");
        }else{
            user = userList.get(0);
        }

        //查询城市
        List<SysRole> sysRoleList = fsSysService.getUserCity(user.getId());
        if(sysRoleList!= null && sysRoleList.size()>0){
            user.setCity(sysRoleList.get(0).getCity());
        }
//        user.setCity(city);
        String token = TokenPre.pc_token_pre+TokenCreate.getToken();
        redisService.put(token, JSONUtil.toJsonStr(user), 60 * 60 * 8); //保存token到redis 设置8个小时后失效

        List<SysMenu> sysMenus = fsSysService.selectUserMenu(user.getId(),1);
        map.put("token", token);
        map.put("userName", user.getName());
        map.put("user", user);
        map.put("city", user.getCity());
        map.put("userMenus", sysMenus);
        return ResultCode.ok().setData(map);
    }


    public ResultCode h5Login(String phone) {
        Map<String,Object> map = new HashMap<>();
        DimEmployeeParam dimEmployeeParam = new DimEmployeeParam();
        dimEmployeeParam.setPhone(phone);
        List<DimEmployeeInfo> list = dimEmployeeInfoService.queryUserByFiled(dimEmployeeParam);
        //账号不存在
        if (list == null||list.size()==0) {
            return ResultCode.error("用户钉钉不在有家系统中");
        }
        SysUser user ;
        SysUserParam sysUserParam = new SysUserParam();
        sysUserParam.setPhone(phone);
        List<SysUser> userList = fsSysService.selectUserSelect(sysUserParam);
        //用户没登录 在表里注册
        if(userList==null||userList.size()==0){
            return ResultCode.error("用户不在系统中，请联系管理人员");
        }else{
            user = userList.get(0);
        }
        String token = TokenPre.h5_token_pre+TokenCreate.getToken();
        redisService.put(token, JSONUtil.toJsonStr(list.get(0)), 60 * 60 * 8); //保存token到redis 设置8个小时后失效
        List<SysMenu> sysMenus = fsSysService.selectUserMenu(user.getId(),2);
        map.put("token", token);
        map.put("user", list.get(0));
        map.put("userMenus", sysMenus);
        return ResultCode.ok().setData(map);
    }
}
