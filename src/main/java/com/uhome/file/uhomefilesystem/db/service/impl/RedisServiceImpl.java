package com.uhome.file.uhomefilesystem.db.service.impl;


import com.uhome.file.uhomefilesystem.db.service.IRedisService;
import org.springframework.stereotype.Service;

/**
 * Created by uh on 2018/9/10.
 */
@Service("redisService")
public class RedisServiceImpl extends IRedisService {

    private static final String REDIS_KEY = "TEST_REDIS_KEY";

    @Override
    protected String getRedisKey() {
        return this.REDIS_KEY;
    }

}