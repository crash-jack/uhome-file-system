package com.uhome.file.uhomefilesystem.db.service;


import com.uhome.file.uhomefilesystem.db.entity.DimEmployeeInfo;
import com.uhome.file.uhomefilesystem.db.param.DimEmployeeParam;
import com.uhome.file.uhomefilesystem.db.vo.HProductDetailVo;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author zbb
 * @since 2020-07-14
 */
public interface IDimEmployeeInfoService {

    List<DimEmployeeInfo> queryUserByFiled(DimEmployeeParam  dimEmployeeParam);
}
