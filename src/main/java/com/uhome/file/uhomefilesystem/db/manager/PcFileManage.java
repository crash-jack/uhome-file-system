package com.uhome.file.uhomefilesystem.db.manager;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uhome.file.uhomefilesystem.common.enums.CertificateDeptEnum;
import com.uhome.file.uhomefilesystem.config.datasources.DataSourceNames;
import com.uhome.file.uhomefilesystem.config.datasources.annotation.DataSource;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.dao.FsCertificateMapper;
import com.uhome.file.uhomefilesystem.db.dao.FsProductDetailMapper;
import com.uhome.file.uhomefilesystem.db.dao.FsStoreDetailMapper;
import com.uhome.file.uhomefilesystem.db.dao.FsStoreListMapper;
import com.uhome.file.uhomefilesystem.db.entity.*;
import com.uhome.file.uhomefilesystem.db.param.*;
import com.uhome.file.uhomefilesystem.db.service.IFsProductDetilService;
import com.uhome.file.uhomefilesystem.db.service.IFsProductListService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreListService;
import com.uhome.file.uhomefilesystem.db.vo.PProductDetailVo;
import com.uhome.file.uhomefilesystem.db.vo.PStoreDetailVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Component
public class PcFileManage {
    @Autowired
    private IFsProductListService fsProductListService;

    @Autowired
    private IFsStoreListService fsStoreListService;
    @Autowired
    private FsStoreListMapper fsStoreListMapper;
    @Autowired
    private FsCertificateMapper fsCertificateMapper;
    @Autowired
    private FsProductDetailMapper fsProductDetailMapper;
    @Autowired
    private FsStoreDetailMapper fsStoreDetailMapper;

    @Value("${upload.imagesAddress}")
    private String imagesAddress;

    public ResultCode queryProductData(FsProductListParam productListParam, PageInfo pageable) {
        PageHelper.startPage(pageable.getPageNum(), pageable.getSize());
        PageInfo<FsProductList> pageInfo = new PageInfo<>(fsProductListService.queryProductList(productListParam));
        List<FsProductList> productList = pageInfo.getList();
        if (productList != null && productList.size() > 0) {
            List<FsCertificate> fsCertificates = fsCertificateMapper.selectByType(CertificateDeptEnum.SHANG_PIN_BU.getCode());
            for (int i = 0; i < productList.size(); i++) {
                List<PProductDetailVo> pProductDetailVoList = fsProductListService.queryProductListDetail(productList.get(i).getId());
                Map<Long, List<PProductDetailVo>> midMap = new HashMap<>();
                Map<Long, MidProductParam> mid2Map = new HashMap<>();
                if (pProductDetailVoList != null && pProductDetailVoList.size() > 0) {
                    for (int j = 0; j < pProductDetailVoList.size(); j++) {
                        List<PProductDetailVo> midp = midMap.get(pProductDetailVoList.get(j).getCertificateId());
                        if (midp != null && midp.size() > 0) {
                            boolean flag = false;
                            for (int k = 0; k < midp.size(); k++) {
                                if (pProductDetailVoList.get(j).getId() != midp.get(k).getId() && pProductDetailVoList.get(j).getCertificateId().equals(midp.get(k).getCertificateId())) {
                                    flag = true;
                                }
                            }
                            if (flag) {
                                midp.add(pProductDetailVoList.get(j));
                                midMap.put(pProductDetailVoList.get(j).getCertificateId(), midp);
                            }
                        } else {
                            MidProductParam midProductParam = new MidProductParam();
                            midProductParam.setId(pProductDetailVoList.get(j).getCertificateId());
                            midProductParam.setName(pProductDetailVoList.get(j).getCertificateName());
                            List<PProductDetailVo> urlList2 = new ArrayList<>();
                            urlList2.add(pProductDetailVoList.get(j));
                            midProductParam.setUrlList(urlList2);
                            midMap.put(pProductDetailVoList.get(j).getCertificateId(), urlList2);
                            mid2Map.put(pProductDetailVoList.get(j).getCertificateId(), midProductParam);
                        }
                    }
                }
                List<MidProductParam> fileList = new ArrayList<>();
                for (int j = 0; j < fsCertificates.size(); j++) {
                    Long key = fsCertificates.get(j).getId();
                    if (midMap.get(key) != null) {
                        MidProductParam midProductParam = new MidProductParam();
                        midProductParam.setId(key);
                        midProductParam.setName(mid2Map.get(key).getName());
                        midProductParam.setUrlList(midMap.get(key));
                        fileList.add(midProductParam);
                    } else {
                        MidProductParam midProductParam = new MidProductParam();
                        midProductParam.setId(key);
                        midProductParam.setName(fsCertificates.get(j).getName());
                        midProductParam.setUrlList(new ArrayList<>());
                        fileList.add(midProductParam);
                    }
                }
                productList.get(i).setFileList(fileList);
            }
        }
        pageInfo.setList(productList);
        return ResultCode.ok().setData(pageInfo);
    }

    public ResultCode queryStoreData(FsStoreListParam storeListParam, PageInfo pageable) {
        PageHelper.startPage(pageable.getPageNum(), pageable.getSize());
        PageInfo<FsStoreList> pageInfo = new PageInfo<>(fsStoreListService.queryStoreList(storeListParam));

        List<FsStoreList> storeList = pageInfo.getList();
        if (storeList != null && storeList.size() > 0) {
            List<FsCertificate> fsCertificates = fsCertificateMapper.selectByType(CertificateDeptEnum.ZHENG_WU_BU.getCode());
            for (int i = 0; i < storeList.size(); i++) {
                List<PStoreDetailVo> pStoreDetailVoList = fsStoreListService.queryStoreListDetail(storeList.get(i).getId());
                Map<Long, List<PStoreDetailVo>> midMap = new HashMap<>();
                Map<Long, MidStoreParam> mid2Map = new HashMap<>();
                if (pStoreDetailVoList != null && pStoreDetailVoList.size() > 0) {
                    for (int j = 0; j < pStoreDetailVoList.size(); j++) {
                        List<PStoreDetailVo> midp = midMap.get(pStoreDetailVoList.get(j).getCertificateId());
                        if (midp != null && midp.size() > 0) {
                            boolean flag = false;
                            for (int k = 0; k < midp.size(); k++) {
                                if (pStoreDetailVoList.get(j).getId() != midp.get(k).getId() && pStoreDetailVoList.get(j).getCertificateId().equals(midp.get(k).getCertificateId())) {
                                    flag = true;
                                }
                            }
                            if (flag) {
                                midp.add(pStoreDetailVoList.get(j));
                                midMap.put(pStoreDetailVoList.get(j).getCertificateId(), midp);
                            }
                        } else {
                            MidStoreParam midStoreParam = new MidStoreParam();
                            midStoreParam.setId(pStoreDetailVoList.get(j).getCertificateId());
                            midStoreParam.setName(pStoreDetailVoList.get(j).getCertificateName());
                            List<PStoreDetailVo> urlList2 = new ArrayList<>();
                            urlList2.add(pStoreDetailVoList.get(j));
                            midStoreParam.setUrlList(urlList2);
                            midMap.put(pStoreDetailVoList.get(j).getCertificateId(), urlList2);
                            mid2Map.put(pStoreDetailVoList.get(j).getCertificateId(), midStoreParam);
                        }
                    }

                }
                List<MidStoreParam> fileList = new ArrayList<>();
                for (int j = 0; j < fsCertificates.size(); j++) {
                    Long key = fsCertificates.get(j).getId();
                    if (midMap.get(key) != null) {
                        MidStoreParam midStoreParam = new MidStoreParam();
                        midStoreParam.setId(key);
                        midStoreParam.setName(mid2Map.get(key).getName());
                        midStoreParam.setUrlList(midMap.get(key));
                        fileList.add(midStoreParam);
                    } else {
                        MidStoreParam midStoreParam = new MidStoreParam();
                        midStoreParam.setId(key);
                        midStoreParam.setName(fsCertificates.get(j).getName());
                        midStoreParam.setUrlList(new ArrayList<>());
                        fileList.add(midStoreParam);
                    }
                }
                storeList.get(i).setFileList(fileList);
            }
        }
        pageInfo.setList(storeList);
        return ResultCode.ok().setData(pageInfo);
    }

    @DataSource(name = DataSourceNames.SECOND)
    public ResultCode saveProductData(FsProductDetailParam fsProductDetailParam) {
        // 将保存的png图片路径，填充到对应字段
        if(StrUtil.isNotBlank(fsProductDetailParam.getImg())){
            String img = fsProductDetailParam.getImg();
            if(img.endsWith(".pdf")){
                int i = img.indexOf(".pdf");
                String substring = img.substring(0, i);
                String pngPath = substring + "_1.png";
                File file = new File(imagesAddress+img);
                if(file.exists()){
                    fsProductDetailParam.setPngImg(pngPath);
                }
            }else {
                // 如果不是pdf,那么就将img路径填充到png路径
                fsProductDetailParam.setPngImg(fsProductDetailParam.getImg());
            }
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (fsProductDetailParam.getId() == null) {
            FsProductDetail fsProductDetail = new FsProductDetail();
            if (Objects.nonNull(fsProductDetailParam.getRemindTime())) {
                try {
                    fsProductDetail.setRemindTime(simpleDateFormat.parse(fsProductDetailParam.getRemindTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            BeanUtils.copyProperties(fsProductDetailParam, fsProductDetail);
            fsProductDetail.setUpdator(fsProductDetailParam.getUpdator());
            fsProductDetail.setProductId(fsProductDetailParam.getProductId());
            fsProductDetail.setCreator(fsProductDetailParam.getUpdator());
            fsProductDetailMapper.insertSelective(fsProductDetail);
        } else {
            FsProductDetail fsProductDetail = new FsProductDetail();
            if (Objects.nonNull(fsProductDetailParam.getRemindTime())) {
                try {
                    fsProductDetail.setRemindTime(simpleDateFormat.parse(fsProductDetailParam.getRemindTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            BeanUtils.copyProperties(fsProductDetailParam, fsProductDetail);
            fsProductDetail.setUpdator(fsProductDetailParam.getUpdator());
            fsProductDetailMapper.updateByPrimaryKeySelective(fsProductDetail);
        }
        FsProductList fsProductList = new FsProductList();
        fsProductList.setId(fsProductDetailParam.getProductId());
        fsProductList.setUpdater(fsProductDetailParam.getUpdator());
        Integer state = fsProductListService.getRecordState(fsProductDetailParam.getProductId(),1);
        fsProductList.setRecordType(state.toString());
        fsProductListService.updateById(fsProductList);

        return ResultCode.ok();
    }

    @DataSource(name = DataSourceNames.SECOND)
    public ResultCode saveStoreData(SysUser user, FsStoreDetailParam fsStoreDetailParam) {
        // 将保存的png图片路径，填充到对应字段
        if (StrUtil.isNotBlank(fsStoreDetailParam.getImg())) {

            String img = fsStoreDetailParam.getImg();
            log.info("图片路径："+img+"   后缀判断"+img.endsWith(".pdf"));
            if (img.endsWith(".pdf")) {
                int i = img.indexOf(".pdf");
                String substring = img.substring(0, i);
                String pngPath = substring + "_1.png";
                File file = new File(imagesAddress+img);
                if (file.exists()) {
                    fsStoreDetailParam.setPngImg(pngPath);
                }
                log.info("图片路径2："+fsStoreDetailParam.getPngImg());
            } else {
                // 如果不是pdf,那么就将img路径填充到png路径
                fsStoreDetailParam.setPngImg(fsStoreDetailParam.getImg());
            }
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (fsStoreDetailParam.getId() == null) {
            FsStoreDetail fsStoreDetail = new FsStoreDetail();
            fsStoreDetail.setUpdator(user.getId());
            fsStoreDetail.setUpdateTime(DateUtil.date());
            if (Objects.nonNull(fsStoreDetailParam.getRemindTime())) {
                try {
                    fsStoreDetail.setRemindTime(simpleDateFormat.parse(fsStoreDetailParam.getRemindTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            BeanUtils.copyProperties(fsStoreDetailParam, fsStoreDetail);
            log.info("图片路径3："+fsStoreDetail.getPngImg());
            fsStoreDetailMapper.insertSelective(fsStoreDetail);
        } else {
            FsStoreDetail fsStoreDetail = new FsStoreDetail();
            fsStoreDetail.setUpdator(user.getId());
            fsStoreDetail.setUpdateTime(DateUtil.date());
            if (Objects.nonNull(fsStoreDetailParam.getRemindTime())) {
                try {
                    fsStoreDetail.setRemindTime(simpleDateFormat.parse(fsStoreDetailParam.getRemindTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            BeanUtils.copyProperties(fsStoreDetailParam, fsStoreDetail);
            fsStoreDetailMapper.updateByPrimaryKeySelective(fsStoreDetail);
        }

        //修改对应列表的编辑人
        FsStoreList record = new FsStoreList();
        record.setId(fsStoreDetailParam.getStoreId());
        record.setUpdater(user.getId());
        Integer state = fsProductListService.getRecordState(fsStoreDetailParam.getStoreId(),2);
        record.setRecordType(state.toString());
        fsStoreListMapper.updateByPrimaryKeySelective(record);
        return ResultCode.ok();
    }

    @DataSource(name = DataSourceNames.SECOND)
    public ResultCode deleteStoreData(FsStoreDetailParam param) {
        FsStoreDetail fsStoreDetail = fsStoreDetailMapper.selectByPrimaryKey(param.getId());
        fsStoreDetailMapper.deleteByPrimaryKey(param.getId());
        FsStoreList record = new FsStoreList();
        record.setId(fsStoreDetail.getStoreId());
        Integer state = fsProductListService.getRecordState(fsStoreDetail.getStoreId(),2);
        record.setRecordType(state.toString());
        record.setUpdater(param.getUpdator());
        fsStoreListMapper.updateByPrimaryKeySelective(record);

        return ResultCode.ok();
    }

    @DataSource(name = DataSourceNames.SECOND)
    public ResultCode deleteProductData(FsProductDetailParam param) {
        FsProductDetail fsProductDetail = fsProductDetailMapper.selectByPrimaryKey(param.getId());
        FsProductList fsProductList = new FsProductList();
        fsProductList.setId(fsProductDetail.getProductId());
        fsProductDetailMapper.deleteByPrimaryKey(param.getId());
        fsProductList.setUpdater(param.getUpdator());
        Integer state = fsProductListService.getRecordState(fsProductDetail.getProductId(),1);
        fsProductList.setRecordType(state.toString());
        fsProductListService.updateById(fsProductList);

        return ResultCode.ok();
    }
}
