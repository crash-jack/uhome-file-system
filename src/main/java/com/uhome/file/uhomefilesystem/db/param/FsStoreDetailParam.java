package com.uhome.file.uhomefilesystem.db.param;

/**
 * 
 *
 * @author zb
 * @date 2021/07/04
 */
public class FsStoreDetailParam {
    /**
     * fs_store_detail表主键ID
     */
    private Long id;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 店铺类型 0.个人 1 公司
     */
    private String storeType;

    /**
     * 证件类型id
     */
    private Long certificateId;

    /**
     * 证书地址
     */
    private String img;

    /**
     * 证书地址
     */
    private String pngImg;

    /**
     * 证件名称
     */
    private String name;

    /**
     * 提醒日期
     */
    private String remindTime;

    /**
     * 统一社会信用代码
     */
    private String code;

    private Long  updator;

    public Long getUpdator() {
        return updator;
    }

    public void setUpdator(Long updator) {
        this.updator = updator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemindTime() {
        return remindTime;
    }

    public void setRemindTime(String remindTime) {
        this.remindTime = remindTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPngImg() {
        return pngImg;
    }

    public void setPngImg(String pngImg) {
        this.pngImg = pngImg;
    }
}