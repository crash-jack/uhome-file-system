package com.uhome.file.uhomefilesystem.db.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsProductQuery;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author bowen.li
 * @since 2021-08-24
 */
@Repository
public interface FsProductQueryMapper extends BaseMapper<FsProductQuery> {

    List<Map<String, Object>> getCountTotalByType(@Param("type") Integer type, @Param("name") String name);

    List<Map<String, Object>> getExportDate(@Param("time") String time);
}
