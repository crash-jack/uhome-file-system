package com.uhome.file.uhomefilesystem.db.vo;

public class HProductVo {
    /**
     *
     */
    private Long id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品编码
     */
    private String norm;
    /**
     * 商品编码
     */
    private String code;

    /**
     * 商品品类
     */
    private String productType;

    /**
     * 商品大类
     */
    private String productCate1;


    public Long getId() {
        return id;
    }

    public String getNorm() {
        return norm;
    }

    public void setNorm(String norm) {
        this.norm = norm;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductCate1() {
        return productCate1;
    }

    public void setProductCate1(String productCate1) {
        this.productCate1 = productCate1;
    }
}
