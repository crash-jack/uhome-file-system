package com.uhome.file.uhomefilesystem.db.service;


import com.uhome.file.uhomefilesystem.db.vo.HProductDetailVo;
import com.uhome.file.uhomefilesystem.db.vo.ProductDetailDingVo;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author zbb
 * @since 2020-07-14
 */
public interface IFsProductDetilService {

    List<HProductDetailVo> queryProductDetailData(Long id);

    List<ProductDetailDingVo> getListByRemindTime();
}
