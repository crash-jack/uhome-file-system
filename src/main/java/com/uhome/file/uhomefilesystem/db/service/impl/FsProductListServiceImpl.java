package com.uhome.file.uhomefilesystem.db.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.uhome.file.uhomefilesystem.config.constant.RecordTypeConstant;
import com.uhome.file.uhomefilesystem.config.datasources.DataSourceNames;
import com.uhome.file.uhomefilesystem.config.datasources.annotation.DataSource;
import com.uhome.file.uhomefilesystem.config.utils.ExportDataUtils;
import com.uhome.file.uhomefilesystem.db.dao.*;
import com.uhome.file.uhomefilesystem.db.entity.FsCertificate;
import com.uhome.file.uhomefilesystem.db.entity.FsProductDetail;
import com.uhome.file.uhomefilesystem.db.entity.FsProductList;
import com.uhome.file.uhomefilesystem.db.param.FsProductListParam;
import com.uhome.file.uhomefilesystem.db.service.IFsProductListService;
import com.uhome.file.uhomefilesystem.db.vo.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author zbb
 * @since 2021-06-19
 */
@RequiredArgsConstructor
@Service
public class FsProductListServiceImpl implements IFsProductListService {

    private final FsProductListMapper fsProductListMapper;

    private final FsProductDetailMapper fsProductDetailMapper;

    private final FsStoreListMapper fsStoreListMapper;

    private final FsStoreDetailMapper fsStoreDetailMapper;

    private final  FsCertificateMapper fsCertificateMapper;

    @Override
    public List<HProductVo> queryProductData(String queryStr) {
        return fsProductListMapper.queryProductData(queryStr);
    }

    @Override
    public List<FsProductList> queryProductList(FsProductListParam productListParam) {
        return fsProductListMapper.queryProductList(productListParam);
    }

    @Override
    public List<PProductDetailVo> queryProductListDetail(Long id) {
        return fsProductListMapper.queryProductListDetail(id);
    }

    @Override
    public Map<String, Set<String>> getCurrUserProductType() {
        List<FsProductList> list = fsProductListMapper.getCurrUserProductType();
        Map<String, Set<String>> map = new HashMap<>(2);
        Set<String> productType = new TreeSet<>();
        Set<String> productCate = new TreeSet<>();
        if (CollUtil.isNotEmpty(list)) {
            for (FsProductList li : list) {
                if (StrUtil.isNotEmpty(li.getProductCate2Name())) {
                    productType.add(li.getProductCate2Name());
                }
                if (StrUtil.isNotEmpty(li.getProductCate1Name())) {
                    productCate.add(li.getProductCate1Name());
                }
            }
        }
        map.put("productType", productType);
        map.put("productCate", productCate);
        return map;
    }

    @Override
    public JSONArray getCurrUserProductTypeTree() {
        List<FsProductList> list = fsProductListMapper.getCurrUserProductType();
        JSONArray array = new JSONArray();
        if (CollUtil.isNotEmpty(list)) {
            for (FsProductList li : list) {
                boolean flag = true;
                for (Object o : array) {
                    JSONObject jsonObject = JSONUtil.parseObj(o);
                    if (jsonObject.getStr("label").equals(li.getProductCate1Name())) {
                        flag = false;
                    }
                }

                if (flag) {
                    JSONObject json = new JSONObject();
                    json.set("label", li.getProductCate1Name());
                    json.set("value", li.getProductCate1Name());

                    Set<String> children = new TreeSet<>();
                    for (FsProductList li1 : list) {
                        if (Objects.equals(li1.getProductCate1(), li.getProductCate1())) {
                            children.add(li1.getProductCate2Name());
                        }
                    }

                    JSONArray array1 = new JSONArray();
                    for (String child : children) {
                        JSONObject ob = new JSONObject();
                        ob.set("label", child);
                        ob.set("value", child);
                        array1.add(ob);
                    }
                    json.set("children", array1);
                    array.add(json);
                }
            }
        }
        return array;
    }

    @Override
    public TotalDataVo getTotalData() {
        //获取总数
        Integer total = fsProductListMapper.selectCount(null, null, null);

        //未录入总数
        Integer notTotal = fsProductListMapper.selectCount(RecordTypeConstant.NOT_ENTERED, null, null);

        //录入的数量
        Integer enTotal = fsProductListMapper.selectCount(RecordTypeConstant.ENTERED, null, null);

        //门店档案录入完成率
        BigDecimal enRate = NumberUtil.div(enTotal, total, 3).multiply(NumberUtil.toBigDecimal(100));

        //未录入占比
        BigDecimal notRate = NumberUtil.div(notTotal, total, 3).multiply(NumberUtil.toBigDecimal(100));

        //录入的占比
        TotalDataVo vo = new TotalDataVo();
        vo.setTotal(total);
        vo.setNotTotal(notTotal);
        vo.setEnTotal(enTotal);
        vo.setEnRate(enRate);
        vo.setNotRate(notRate);
        vo.setInputRate(enRate);
        return vo;
    }

    @Override
    public TotalBaseVo getTypeEnRate(Integer type, String name) {
        //查询区域 完成总数
        Integer total = fsProductListMapper.selectCount(RecordTypeConstant.ENTERED, type, name);

        //查询区域 未录入总数
        Integer notTotal = fsProductListMapper.selectCount(RecordTypeConstant.NOT_ENTERED, type, name);

        //查询区域 部分录入总数
        Integer partTotal = fsProductListMapper.selectCount(RecordTypeConstant.PARTIAL_ENTRY, type, name);

        TotalBaseVo vo = new TotalBaseVo();
        vo.setTotal(total);
        vo.setNotTotal(notTotal);
        vo.setPartTotal(partTotal);
        return vo;
    }

    @Override
    public TotalEnRateVo getTypeRate(Integer type, String name) {
        List<Map<String, Object>> data = fsProductListMapper.getTypeEnRate(type, name);
        Set<String> typeList = data.stream().map(item -> MapUtil.getStr(item, "productType")).collect(Collectors.toSet());
        List<Integer> totalRates = new ArrayList<>();
        List<Integer> notTotalRates = new ArrayList<>();
        List<Integer> partTotalRates = new ArrayList<>();
        for (String li : typeList) {
            totalRates.add(getProductEnRateNumber(data, li, RecordTypeConstant.ENTERED));
            notTotalRates.add(getProductEnRateNumber(data, li, RecordTypeConstant.NOT_ENTERED));
            partTotalRates.add(getProductEnRateNumber(data, li, RecordTypeConstant.PARTIAL_ENTRY));
        }

        TotalEnRateVo vo = new TotalEnRateVo();
        vo.setType(typeList);
        vo.setTotalRates(totalRates);
        vo.setNotTotalRates(notTotalRates);
        vo.setPartRates(partTotalRates);
        return vo;
    }

    private Integer getProductEnRateNumber(List<Map<String, Object>> enRate, String productType, int type) {
        return enRate.stream().filter(item ->
                        MapUtil.getStr(item, "productType").equals(productType)
                                &&
                                MapUtil.getInt(item, "recordType").equals(type))
                .mapToInt(item -> MapUtil.getInt(item, "number", 0)).sum();
    }

    @Override
    public void export(int type, HttpServletResponse response) {

        List<FsProductList> list = fsProductListMapper.selectList(ExportDataUtils.exportTypeToData(type));
        ArrayList<Map<String, Object>> courseDtoList = new ArrayList<>();
        for (FsProductList li : list) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("商品名称", li.getName());
            map.put("商品编码", li.getCode());
            map.put("商品规格", li.getNorm());
            map.put("商品品类", li.getProductType());
            map.put("商品状态", ExportDataUtils.getProductState(li.getState()));
            map.put("录入状态", ExportDataUtils.getRecordType(li.getRecordType()));
            courseDtoList.add(map);
        }
        ServletOutputStream ouPutStream = null;
        //导出成excel
        try (ExcelWriter writer = ExcelUtil.getWriter()) {
            ExportDataUtils.setWriterResponse(response, DateUtil.now());
            if (CollUtil.isNotEmpty(courseDtoList)) {
                writer.write(courseDtoList, true);
            } else {
                List<String> head = new ArrayList<>();
                head.add("商品名称");
                head.add("商品编码");
                head.add("商品规格");
                head.add("商品品类");
                head.add("商品状态");
                head.add("录入状态");
                writer.writeHeadRow(head);
            }
            ouPutStream = response.getOutputStream();
            writer.flush(ouPutStream, true);
        } catch (IOException e) {
            throw new RuntimeException("导出失败");
        } finally {
            IoUtil.close(ouPutStream);
        }
    }


    @Override
    public ProductDetailDingVo getDingById(String id) {
        return fsProductListMapper.getDingById(id);
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public int updateById(FsProductList fsProductList) {
        return fsProductListMapper.updateByPrimaryKeySelective(fsProductList);
    }

    @Override
    public int getRecordState(Long id, Integer type) {
        Integer state  = 0; //0.未录入 1.部分录入 2.已录入
        List<FsCertificate> fsCertificates =fsCertificateMapper.getDa();
        if(type==1){
            //商品
            int number = 0;
            int hasNumber = 0;
            List<HProductDetailVo> list = fsProductDetailMapper.queryProductDetailData(id);
            if(list!= null && list.size()>0){
                CerFor:
                for (int i = 0; i < fsCertificates.size(); i++) {
                    //证件类型  1 商品部  2 政务部
                    if(fsCertificates.get(i).getType().equals("1")){
                        hasNumber++;
                        for (int j = 0; j < list.size(); j++) {
                            if(list.get(j).getCertificateId()==fsCertificates.get(i).getId()&&"0".equals(list.get(j).getIsDeleted())){
                                number++;
                                continue CerFor;
                            }
                        }
                    }
                }
                if (number==0){
                    state  = 0;
                }else if(number==hasNumber){
                    state = 2;
                }else{
                    state =1;
                }
            }
        }else{
            //门店
            int number = 0;
            int hasNumber = 0;
            List<HStoreDetailVo> list = fsStoreDetailMapper.queryStoreDetailData(id);
            if(list!= null && list.size()>0){
                CerFor:
                for (int i = 0; i < fsCertificates.size(); i++) {
                    //证件类型  1 商品部  2 政务部
                    if(fsCertificates.get(i).getType().equals("2")){
                        hasNumber++;
                        for (int j = 0; j < list.size(); j++) {
                            if(list.get(j).getCertificateId()==fsCertificates.get(i).getId()&&"0".equals(list.get(j).getIsDeleted())){
                                number++;
                                continue CerFor;
                            }
                        }
                    }
                }
                if (number==0){
                    state  = 0;
                }else if(number==hasNumber){
                    state = 2;
                }else{
                    state =1;
                }
            }

        }
        return state;
    }
}
