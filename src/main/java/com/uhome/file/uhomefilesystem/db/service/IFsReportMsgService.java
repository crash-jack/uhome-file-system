package com.uhome.file.uhomefilesystem.db.service;

import java.util.Map;

public interface IFsReportMsgService {

    Integer reportMsg(Map<String, Object> map);
}
