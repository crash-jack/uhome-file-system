package com.uhome.file.uhomefilesystem.db.param;

/**
 * 
 *
 * @author zb
 * @date 2021/07/04
 */
public class DimEmployeeParam {

    /**
     * 名称
     */
    private String name;

    /**
     * 手机
     */
    private String phone;

    /**
     * 部门
     */
    private String department;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}