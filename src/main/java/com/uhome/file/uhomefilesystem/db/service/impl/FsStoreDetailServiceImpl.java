package com.uhome.file.uhomefilesystem.db.service.impl;

import com.uhome.file.uhomefilesystem.config.datasources.DataSourceNames;
import com.uhome.file.uhomefilesystem.config.datasources.annotation.DataSource;
import com.uhome.file.uhomefilesystem.db.dao.FsStoreDetailMapper;
import com.uhome.file.uhomefilesystem.db.entity.FsStoreDetail;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreDetailService;
import com.uhome.file.uhomefilesystem.db.vo.HStoreDetailVo;
import com.uhome.file.uhomefilesystem.db.vo.StoreDetailDingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author zbb
 * @since 2021-06-19
 */
@Service
public class FsStoreDetailServiceImpl implements IFsStoreDetailService {

    @Autowired
    FsStoreDetailMapper fsStoreDetailMapper;


    @Override
    public List<HStoreDetailVo> queryStoreDetailData(Long id) {
        return fsStoreDetailMapper.queryStoreDetailData(id);
    }

    @Override
    public List<StoreDetailDingVo> getListByRemindTime() {
        return fsStoreDetailMapper.getListByRemindTime();
    }

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public int insertSelective(FsStoreDetail record) {
        return fsStoreDetailMapper.insertSelective(record);
    }


}
