package com.uhome.file.uhomefilesystem.db.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TotalDataVo {

    /**
     * 总数
     */
    private Integer total;

    /**
     * 未录入总数
     */
    private Integer notTotal;

    /**
     * 录入的数量
     */
    private Integer enTotal;

    /**
     * 档案录入完成率
     */
    private BigDecimal enRate;

    /**
     * 未录入占比
     */
    private BigDecimal notRate;

    /**
     * 录入的占比
     */
    private BigDecimal inputRate;
}
