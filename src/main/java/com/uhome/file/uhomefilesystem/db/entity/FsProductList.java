package com.uhome.file.uhomefilesystem.db.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.uhome.file.uhomefilesystem.db.param.MidProductParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 *
 * @author zb
 * @date 2021/07/07
 */
public class FsProductList {
    /**
     * 
     */
    private Long id;

    private String gid;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品编码
     */
    private String code;

    /**
     * 商品编码
     */
    private String code2;

    /**
     * 商品规格
     */
    private String norm;

    /**
     * 商品品类
     */
    private String productType;

    /**
     * 商品大类
     */
    @TableField(value = "product_cate_1")
    private String productCate1;

    /**
     * 商品大类名称
     */
    @TableField(value = "product_cate_1_name")
    private String productCate1Name;

    /**
     * 商品大类名称
     */
    @TableField(value = "product_cate_2_name")
    private String productCate2Name;

    /**
     * 商品状态
     */
    private String state;

    /**
     * 录入状态  0.未录入 1.已录入
     */
    private String recordType;

    /**
     * 
     */
    private Long creater;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Long updater;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 编辑人
     */
    @TableField(exist = false)
    private String editor;

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    @TableField(exist = false)
    private List<MidProductParam> fileList = new ArrayList<>();


    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNorm() {
        return norm;
    }

    public void setNorm(String norm) {
        this.norm = norm;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductCate1() {
        return productCate1;
    }

    public void setProductCate1(String productCate1) {
        this.productCate1 = productCate1;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public Long getCreater() {
        return creater;
    }

    public void setCreater(Long creater) {
        this.creater = creater;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUpdater() {
        return updater;
    }

    public void setUpdater(Long updater) {
        this.updater = updater;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public List<MidProductParam> getFileList() {
        return fileList;
    }

    public void setFileList(List<MidProductParam> fileList) {
        this.fileList = fileList;
    }

    public String getProductCate1Name() {
        return productCate1Name;
    }

    public void setProductCate1Name(String productCate1Name) {
        this.productCate1Name = productCate1Name;
    }

    public String getProductCate2Name() {
        return productCate2Name;
    }

    public void setProductCate2Name(String productCate2Name) {
        this.productCate2Name = productCate2Name;
    }
}