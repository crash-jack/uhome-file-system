package com.uhome.file.uhomefilesystem.db.manager;

import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.service.IFsProductDetilService;
import com.uhome.file.uhomefilesystem.db.service.IFsProductListService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreDetailService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreListService;
import com.uhome.file.uhomefilesystem.db.vo.HProductDetailVo;
import com.uhome.file.uhomefilesystem.db.vo.HProductVo;
import com.uhome.file.uhomefilesystem.db.vo.HStoreDetailVo;
import com.uhome.file.uhomefilesystem.db.vo.HStoreVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class HFileManage {
    @Autowired
    private IFsProductListService fsProductListService;

    @Autowired
    private IFsProductDetilService fsProductDetilService;

    @Autowired
    private IFsStoreListService fsStoreListService;

    @Autowired
    private IFsStoreDetailService storeDetailService;

    public ResultCode queryProductData(String queryStr) {
        Map<String,Object> map = new HashMap<>();
        List<HProductVo> productVo = fsProductListService.queryProductData(queryStr);
        if(productVo==null || productVo.size()==0 ||productVo.get(0)==null ||productVo.get(0).getId()==null){
            return ResultCode.error("商品不存在");
        }
        //查询商品证书
        List<HProductDetailVo> detailVoList =  fsProductDetilService.queryProductDetailData(productVo.get(0).getId());
        map.put("product",productVo.get(0));
        map.put("detailList",detailVoList);
        return ResultCode.ok().setData(map);
    }

    public ResultCode queryStoreData(String queryStr) {
        Map<String,Object> map = new HashMap<>();
        List<HStoreVo> storeVo = fsStoreListService.queryStoreData(queryStr);
        if(storeVo==null ||storeVo.size()==0 || storeVo.get(0)==null ||storeVo.get(0).getId()==null){
            return ResultCode.error("商品不存在");
        }
        //查询商品证书
        List<HStoreDetailVo> detailVoList =  storeDetailService.queryStoreDetailData(storeVo.get(0).getId());
        map.put("store",storeVo.get(0));
        map.put("detailList",detailVoList);
        return ResultCode.ok().setData(map);
    }
}
