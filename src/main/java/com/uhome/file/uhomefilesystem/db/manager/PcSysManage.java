package com.uhome.file.uhomefilesystem.db.manager;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.dao.FsCertificateMapper;
import com.uhome.file.uhomefilesystem.db.dao.SysUserMapper;
import com.uhome.file.uhomefilesystem.db.entity.*;
import com.uhome.file.uhomefilesystem.db.param.OprateParam;
import com.uhome.file.uhomefilesystem.db.param.SysRoleParam;
import com.uhome.file.uhomefilesystem.db.param.SysUserParam;
import com.uhome.file.uhomefilesystem.db.service.IFsSysService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class PcSysManage {
    @Autowired
    private IFsSysService sysService;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private FsCertificateMapper fsCertificateMapper;

    public ResultCode getDA() {
        List<FsCertificate> fsCertificates = sysService.getDa();
        return ResultCode.ok().setData(fsCertificates);
    }

    public ResultCode addCer(String name,String type) {
        sysService.addCer(name,type);
        return ResultCode.ok();
    }

    public ResultCode selectMenuList() {
        List<SysMenu> sysMenus = sysService.selectMenuList();
        return ResultCode.ok().setData(sysMenus);
    }

    public ResultCode deleteCer(Long id) {
        sysService.deleteCer(id);
        return ResultCode.ok();
    }

    public ResultCode selectRoleList() {
        List<SysRole> sysRoleList =  sysService.selectRole();
        return ResultCode.ok().setData(sysRoleList);
    }

    public ResultCode selectRoleListWithMenu() {
        List<SysRole> sysRoleList =  sysService.selectRole();
        if(sysRoleList!=null && sysRoleList.size()>0){
            for (int i = 0; i < sysRoleList.size(); i++) {
                List<SysRoleMenu> sysRoleMenus =  sysService.selectRoleMenu(sysRoleList.get(i).getId());
                sysRoleList.get(i).setSysRoleMenus(sysRoleMenus);
            }
        }
        return ResultCode.ok().setData(sysRoleList);
    }

    public ResultCode deleteRole(List<Long> id) {
        for (int i = 0; i < id.size(); i++) {
            sysService.deleteRole(id.get(i));
        }
        return ResultCode.ok();
    }

    public ResultCode saveRole(SysRoleParam sysRoleParam) {
        //添加判断名称是否重复
        if(sysRoleParam.getId()==null){
            List<SysRole> roleList = sysService.selectRoleByName(sysRoleParam.getName());
            if(roleList!=null && roleList.size()>0){
                return ResultCode.error("角色名称重复");
            }
        }

        sysService.saveRole(sysRoleParam);
        return ResultCode.ok();
    }

    public ResultCode selectUser(SysUserParam sysUserParam,PageInfo pageable) {
        Map<String, Object> map = new HashMap<>();
        PageHelper.startPage(pageable.getPageNum(), pageable.getPageSize());
        PageInfo<SysUser> pageInfo = new PageInfo<>(sysService.selectUser(sysUserParam));
        return ResultCode.ok().setData(pageInfo);
    }

    public ResultCode selectUser2(SysUserParam sysUserParam,PageInfo pageable) {
        Map<String, Object> map = new HashMap<>();
        PageHelper.startPage(pageable.getPageNum(), pageable.getPageSize());
        PageInfo<DimEmployeeInfo> pageInfo = new PageInfo<>(sysService.selectUser2(sysUserParam));
        return ResultCode.ok().setData(pageInfo);
    }

    public ResultCode deleteUser(List<Long> ids) {
        for (int i = 0; i < ids.size(); i++) {
            sysService.deleteUser(ids.get(i));
        }
        return ResultCode.ok();
    }

    public ResultCode saveUser(SysUserParam sysUserParam) {
        if(sysUserParam.getUserIds()!=null && sysUserParam.getUserIds().size()>0){
            //这里需要先查询一边用户表是否存在
            List<DimEmployeeInfo> sysUsers = sysService.selectUser2ByIds(sysUserParam.getUserIds());
            if(sysUsers!=null && sysUsers.size()>0){
                for (int i = 0; i < sysUsers.size(); i++) {
                    DimEmployeeInfo sysUserM = sysUsers.get(i);
//                    sysUserM.setId(null);
                    SysUserParam sysUserParam1 = new SysUserParam();
                    sysUserParam1.setName(sysUserM.getName());
                    sysUserParam1.setPhone(sysUserM.getMobile());
                    List<SysUser> users = sysUserMapper.selectUserSelect(sysUserParam1);
                    Long userId = 0L;
                    if (users != null && users.size() > 0) {
                        //用户已存在  不保存
                        userId = users.get(0).getId();
                    } else {
                        SysUser sysUser = new SysUser();
                        sysUser.setDdUserId(sysUserM.getUserId());
                        sysUser.setName(sysUserM.getName());
                        sysUser.setDeptName(sysUserM.getDepartment());
                        sysUser.setPhone(sysUserM.getMobile());
                        sysUser.setState("0");
                        sysUser.setRoleId(sysUserParam.getRoleId());
                        sysService.insertUser(sysUser);
                        userId = sysUser.getId();
                    }
                    //删除用户角色 重新添加
                    sysService.updateRole(userId,sysUserParam.getRoleId());
                }
            }
        }
        return ResultCode.ok();
    }

    public ResultCode queryCertificate(String certificateType) {
        List<FsCertificate> fsCertificates = fsCertificateMapper.selectByType(certificateType);
        return ResultCode.ok().setData(fsCertificates);
    }

    public ResultCode saveLoginLog(FsLoginLog fsLoginLog) {

        return sysService.saveLoginLog(fsLoginLog);
    }

    public ResultCode saveOprateLog(OprateParam oprateParam) {

        return sysService.saveOprateLog(oprateParam);
    }
}
