package com.uhome.file.uhomefilesystem.db.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.uhome.file.uhomefilesystem.config.constant.RecordTypeConstant;
import com.uhome.file.uhomefilesystem.config.constant.ZengshuEnum;
import com.uhome.file.uhomefilesystem.config.datasources.DataSourceNames;
import com.uhome.file.uhomefilesystem.config.datasources.annotation.DataSource;
import com.uhome.file.uhomefilesystem.config.utils.ExportDataUtils;
import com.uhome.file.uhomefilesystem.config.utils.PdfUtil;
import com.uhome.file.uhomefilesystem.db.dao.*;
import com.uhome.file.uhomefilesystem.db.entity.*;
import com.uhome.file.uhomefilesystem.db.param.FsStoreListParam;
import com.uhome.file.uhomefilesystem.db.service.IFsProductListService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreDetailService;
import com.uhome.file.uhomefilesystem.db.service.IFsStoreListService;
import com.uhome.file.uhomefilesystem.db.vo.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author zbb
 * @since 2021-06-19
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class FsStoreListServiceImpl implements IFsStoreListService {

    @Autowired
    private IFsProductListService fsProductListService;
    private final FsStoreListMapper fsStoreListMapper;
    private final FsStoreDetailMapper fsStoreDetailMapper;
    private final FsCertificateMapper fsCertificateMapper;
    private final FsProductListMapper fsProductListMapper;
    private final FsProductDetailMapper fsProductDetailMapper;

    private String [] zhengshu = {"营业执照","烟草证","食品证"};


    @Value("${upload.imagesAddress}")
    private String imagesAddress;

    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void initStore() {
        String path = "F:\\download\\googleDownload\\下载的文件\\store-add";
        String imgpath = "F:\\download\\googleDownload\\下载的文件\\store-add\\";
        log.info("开始获取名称" );
        List<String> fileNames = getFiles(path);
        log.info("获取名称" );
        try {
            List<FsStoreList> storeLists = fsStoreListMapper.selectList(null, null);
            log.info("获取storeLists"+storeLists.size() );
            List<String> errorList = new ArrayList<>();
            List<FsStoreDetail> saveData = new ArrayList<>();
            List<FsStoreDetail> pdfData = new ArrayList<>();
            for (int i = 0; i < fileNames.size(); i++) {
                String [] arrName = fileNames.get(i).split("\\.");
                String [] arr = null;
                if(arrName[0].contains("-")){
                    arr = arrName[0].split("-");
                }
                if(arrName[0].contains("_")){
                    arr = arrName[0].split("_");
                }

                FsStoreList currentStore = null;
                //门店名称去除‘店’
                String getStoreName = arr[1].substring(0,arr[1].length()-1);

                //校验数据
                if(arr.length==3){
                    int num = 0;
                    for (int j = 0; j < storeLists.size(); j++) {
                        if(storeLists.get(j).getName().contains(getStoreName)){
                            currentStore = storeLists.get(j);
                            num++;
                        }
                    }
                    if(currentStore==null){
                        errorList.add(fileNames.get(i));
                        log.info("查询不到的门店："+fileNames.get(i));
                        continue;
                    }
                    if(num>1){
                        errorList.add(fileNames.get(i));
                        log.info("重名的门店："+fileNames.get(i));
                        continue;
                    }

                    boolean arr3Flag = false;
                    for (int j = 0; j < zhengshu.length ; j++) {
                        if(arr[2].equals(zhengshu[j] )){
                            arr3Flag = true;
                        }
                    }
                    if(!arr3Flag){
                        errorList.add(fileNames.get(i));
                        continue;
                    }
                    ZengshuEnum cid =  ZengshuEnum.getValue(arr[2]);
                    if(cid==null){
                        errorList.add(fileNames.get(i));
                        continue;
                    }
                    FsStoreDetail fsStoreDetail = new FsStoreDetail();
                    fsStoreDetail.setName(fileNames.get(i));
                    fsStoreDetail.setStoreId(currentStore.getId());
                    fsStoreDetail.setCertificateId(Long.parseLong(cid.getKey().toString()));
                    fsStoreDetail.setStoreType(currentStore.getStoreType());
                    fsStoreDetail.setImg(imgpath + fileNames.get(i));
                    if (arrName[1].equals("pdf") || arrName[1].equals("PDF")) {
                        pdfData.add(fsStoreDetail);
                        fsStoreDetail.setPngImg(imgpath + arrName[0] + "_1.png");
                    }else{
                        fsStoreDetail.setPngImg(imgpath + fileNames.get(i));
                    }
                    saveData.add(fsStoreDetail);
                } else {
                    errorList.add(fileNames.get(i));
                    continue;
                }
            }
            log.info("准备保存" );
            //保存
            int number = 0;
            for (int i = 0; i < saveData.size(); i++) {
                FsStoreDetail fsStoreDetail = saveData.get(i);
                int q = fsStoreDetailMapper.insertSelective(fsStoreDetail);
                FsStoreList record = new FsStoreList();
                record.setId(fsStoreDetail.getStoreId());
                Integer state = fsProductListService.getRecordState(fsStoreDetail.getStoreId(),2);
                record.setRecordType(state.toString());
                fsStoreListMapper.updateByPrimaryKeySelective(record);
                number = number + q;
            }
            log.info("成功的数据：" + number);
            for (int i = 0; i < errorList.size(); i++) {
                log.info("失败的数据：" + errorList.get(i));
            }
//            for (int i = 0; i < errorList.size(); i++) {
//                copyFile(path+"\\"+errorList.get(i),"F:\\download\\googleDownload\\下载的文件\\errorStore\\"+errorList.get(i));
//            }
            //pdf转图片
//            if (pdfData != null && pdfData.size() > 0) {
//                for (int i = 0; i < pdfData.size(); i++) {
//                    PdfUtil.pdfToPng(pdfData.get(i).getImg(), 30);
//                    log.info("pdf转换成功：" + pdfData.get(i).getImg());
//                }
//            }
        }catch (Exception e){
            e.printStackTrace();
            log.info(e.getMessage());
        }
    }

    public void copyFile(String oldPath, String newPath) {
        try {
            int bytesum = 0;
            int byteread = 0;
            File oldfile = new File(oldPath);
            if (oldfile.exists()) { //文件存在时
                InputStream inStream = new FileInputStream(oldPath); //读入原文件
                FileOutputStream fs = new FileOutputStream(newPath);
                byte[] buffer = new byte[1444];
                int length;
                while ( (byteread = inStream.read(buffer)) != -1) {
                    bytesum += byteread; //字节数 文件大小
                    System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                inStream.close();
            }
        }
        catch (Exception e) {
            System.out.println("复制单个文件操作出错");
            e.printStackTrace();

        }

    }
    @Override
    @DataSource(name = DataSourceNames.SECOND)
    public void initGoods() {
        String path = "F:\\download\\googleDownload\\下载的文件\\goods\\11";
        String imgpath = "F:\\download\\googleDownload\\下载的文件\\goods\\11\\";
        log.info("开始获取名称" );
        List<String> fileNames = getFiles(path);
        log.info("获取名称" );
        try {
            List<FsProductList> storeLists = fsProductListMapper.selectList(null);
            log.info("获取storeLists"+storeLists.size() );
            List<String> errorList = new ArrayList<>();
            List<FsProductDetail> saveData = new ArrayList<>();
            List<FsProductDetail> pdfData = new ArrayList<>();
            for (int i = 0; i < fileNames.size(); i++) {
                String [] arrName = fileNames.get(i).split("\\.");
                FsProductList currentStore = null;
                //校验数据
                if(arrName.length==2){
                    for (int j = 0; j < storeLists.size(); j++) {
                        if(arrName[0].equals(storeLists.get(j).getCode2())){
                            currentStore = storeLists.get(j);
                        }
                    }
                    if(currentStore==null){
                        errorList.add(fileNames.get(i));
                        continue;
                    }
                    FsProductDetail fsStoreDetail = new FsProductDetail();
                    fsStoreDetail.setName(currentStore.getName()+"商品证");
                    fsStoreDetail.setProductId(currentStore.getId());
                    fsStoreDetail.setCertificateId(1l);
                    fsStoreDetail.setImg(imgpath + fileNames.get(i));
                    if (arrName[1].equals("pdf") || arrName[1].equals("PDF")) {
                        pdfData.add(fsStoreDetail);
                        fsStoreDetail.setPngImg(imgpath + arrName[0] + "_1.png");
                    }else{
                        fsStoreDetail.setPngImg(imgpath + fileNames.get(i));
                    }
                    saveData.add(fsStoreDetail);
                } else {
                    errorList.add(fileNames.get(i));
                    continue;
                }
            }
            log.info("准备保存" );
            //保存
//            int number = 0;
//            for (int i = 0; i < saveData.size(); i++) {
//                FsProductDetail fsStoreDetail = saveData.get(i);
//                int q = fsProductDetailMapper.insertSelective(fsStoreDetail);
//                FsProductList fsProductList = new FsProductList();
//                fsProductList.setId(fsStoreDetail.getProductId());
//                Integer state = fsProductListService.getRecordState(fsStoreDetail.getProductId(),1);
//                fsProductList.setRecordType(state.toString());
//                fsProductListService.updateById(fsProductList);
//                number = number + q;
//            }
//            log.info("成功的数据：" + number);
//            for (int i = 0; i < errorList.size(); i++) {
//                log.info("失败的数据：" + errorList.get(i));
//            }

            //pdf转图片
            if (pdfData != null && pdfData.size() > 0) {
                for (int i = 0; i < pdfData.size(); i++) {
                    PdfUtil.pdfToPng(pdfData.get(i).getImg(), 30);
                    log.info("pdf转换成功：" + pdfData.get(i).getImg());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            log.info(e.getMessage());
        }
    }

    @Override
    public void initGoodsImg() {

    }

    @Override
    public void initStoreImg() {
        String path = "/filesystem/2021/moban/store";
        log.info("开始获取名称" );
        List<String> fileNames = getFiles(imagesAddress+path);
        log.info("获取名称" );
        try {
            List<String> errorList = new ArrayList<>();
            List<FsStoreDetail> saveData = new ArrayList<>();
            List<FsStoreDetail> pdfData = new ArrayList<>();
            for (int i = 0; i < fileNames.size(); i++) {
                String [] arrName = fileNames.get(i).split("\\.");
                String [] arr = arrName[0].split("-");
                //校验数据
                if(arr.length==3){
                    boolean arr3Flag = false;
                    for (int j = 0; j < zhengshu.length ; j++) {
                        if(arr[2].equals(zhengshu[j] )){
                            arr3Flag = true;
                        }
                    }
                    if(!arr3Flag){
                        errorList.add(fileNames.get(i));
                        continue;
                    }
                    ZengshuEnum cid =  ZengshuEnum.getValue(arr[2]);
                    if(cid==null){
                        errorList.add(fileNames.get(i));
                        continue;
                    }
                    FsStoreDetail fsStoreDetail = new FsStoreDetail();
                    fsStoreDetail.setName(fileNames.get(i));
                    fsStoreDetail.setCertificateId(Long.parseLong(cid.getKey().toString()));
                    fsStoreDetail.setImg(path + arrName[0] + arrName[1]);
                    if (arrName[1].equals("pdf") || arrName[1].equals("PDF")) {
                        pdfData.add(fsStoreDetail);
                        fsStoreDetail.setPngImg(path + arrName[0] + "_1.png");
                    }else{
                        fsStoreDetail.setPngImg(path + arrName[0] + arrName[1]);
                    }
                } else {
                    errorList.add(fileNames.get(i));
                    continue;
                }
            }
            //pdf转图片
            if (pdfData != null && pdfData.size() > 0) {
                for (int i = 0; i < pdfData.size(); i++) {
                    PdfUtil.pdfToPng(pdfData.get(i).getImg(), 30);
                    log.info("pdf转换成功：" + pdfData.get(i).getImg());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            log.info(e.getMessage());
        }
    }

    /**
     * 递归获取某路径下的所有文件，文件夹，并输出
     */

    public static List<String> getFiles(String path) {
        List<String> fileNames = new ArrayList<>();
        File file = new File(path);
// 如果这个路径是文件夹
        if (file.isDirectory()) {
// 获取路径下的所有文件
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
// 如果还是文件夹 递归获取里面的文件 文件夹
                if (files[i].isDirectory()) {
                    getFiles(files[i].getPath());
                } else {
                    fileNames.add(files[i].getName());
                }
            }
        } else {
            fileNames.add(file.getName());
        }
        return fileNames;
    }


    @Override
    public List<HStoreVo> queryStoreData(String queryStr) {
        return fsStoreListMapper.queryStoreData(queryStr);
    }

    @Override
    public List<FsStoreList> queryStoreList(FsStoreListParam storeListParam) {
        return fsStoreListMapper.queryStoreList(storeListParam);
    }

    @Override
    public List<PStoreDetailVo> queryStoreListDetail(Long id) {
        return fsStoreListMapper.queryStoreListDetail(id);
    }

    @Override
    public StoreCityAreaVo getCurrUserCityAndArea(SysUser user) {

        List<FsStoreList> fsStoreLists = fsStoreListMapper.selectListGroup(user.getCity().split(","), null);

        StoreCityAreaVo vo = new StoreCityAreaVo();
        if (CollUtil.isNotEmpty(fsStoreLists)) {
            Set<String> cityList = new TreeSet<>();
            Set<String> areaList = new TreeSet<>();
            for (FsStoreList fsStoreList : fsStoreLists) {
                if (StrUtil.isNotEmpty(fsStoreList.getCity())) {
                    cityList.add(fsStoreList.getCity());
                }
                if (StrUtil.isNotEmpty(fsStoreList.getArea())) {
                    areaList.add(fsStoreList.getArea());
                }
            }
            vo.setCity(cityList);
            vo.setArea(areaList);
        }
        return vo;
    }

    @Override
    public JSONArray getCurrUserCityAndAreaForPc(SysUser user) {

        List<FsStoreList> list = fsStoreListMapper.selectListGroup(user.getCity().split(","), null);
        JSONArray array = new JSONArray();
        if (CollUtil.isNotEmpty(list)) {
            for (FsStoreList li : list) {
                boolean flag = true;
                for (Object o : array) {
                    JSONObject jsonObject = JSONUtil.parseObj(o);
                    if (jsonObject.getStr("label").equals(li.getCity())) {
                        flag = false;
                    }
                }

                if (flag) {
                    JSONObject json = new JSONObject();
                    json.set("label", li.getCity());
                    json.set("value", li.getCity());

                    Set<String> children = new TreeSet<>();
                    for (FsStoreList li1 : list) {
                        if (Objects.equals(li1.getCity(), li.getCity()) && StringUtils.isNotBlank(li1.getArea())) {
                            children.add(li1.getArea());
                        }
                    }

                    JSONArray array1 = new JSONArray();
                    for (String child : children) {
                        JSONObject ob = new JSONObject();
                        ob.set("label", child);
                        ob.set("value", child);
                        array1.add(ob);
                    }
                    json.set("children", array1);
                    array.add(json);
                }
            }
        }
        return array;
    }

    @Override
    public TotalDataVo getTotalData(String city) {
        String [] arrs =null;
        if(StringUtils.isNotBlank(city)){
            arrs = city.split(",");
        }
        Integer total = 0;
        Integer notTotal = 0;
        Integer enTotal = 0;
        for (int i = 0; i < arrs.length; i++) {
            //获取门店总数
            Integer totalsub = fsStoreListMapper.selectCount(null, arrs[i], null, null);
            total = total + totalsub;
            //未录入总数
            Integer notTotalsub = fsStoreListMapper.selectCount(RecordTypeConstant.NOT_ENTERED, arrs[i], null, null);
            notTotal = notTotal + notTotalsub;
            //录入的数量
            Integer enTotalsub = fsStoreListMapper.selectCount(RecordTypeConstant.ENTERED, arrs[i], null, null);
            enTotal = enTotal + enTotalsub;
        }


        //获取档案的类型
        List<FsCertificate> da = fsCertificateMapper.getDa();
        long daCount = da.stream().filter(item -> item.getType() != null && item.getType().equals("2")).count();

        //门店档案录入完成率
        BigDecimal enRate = NumberUtil.div(enTotal, total, 3).multiply(NumberUtil.toBigDecimal(100));

        //未录入占比
        BigDecimal notRate = NumberUtil.div(notTotal, Convert.toBigDecimal(total * daCount), 3).multiply(NumberUtil.toBigDecimal(100));

        //录入的占比
        TotalDataVo vo = new TotalDataVo();
        vo.setTotal(total);
        vo.setNotTotal(notTotal);
        vo.setEnTotal(enTotal);
        vo.setEnRate(enRate);
        vo.setNotRate(notRate);
        vo.setInputRate(enRate);
        return vo;
    }

    @Override
    public TotalBaseVo getAreaEnRate(SysUser user, String city, String area) {
        //查询区域 完成总数
        String finalCity = setCity(city);
        Integer total = fsStoreListMapper.selectCount(RecordTypeConstant.ENTERED, finalCity, user.getCity(), area);

        //查询区域 未录入总数
        Integer notTotal = fsStoreListMapper.selectCount(RecordTypeConstant.NOT_ENTERED, finalCity, user.getCity(), area);

        //查询区域 部分录入总数
        Integer partTotal = fsStoreListMapper.selectCount(RecordTypeConstant.PARTIAL_ENTRY, finalCity, user.getCity(), area);

        TotalBaseVo vo = new TotalBaseVo();
        vo.setTotal(total);
        vo.setNotTotal(notTotal);
        vo.setPartTotal(partTotal);
        return vo;
    }

    @Override
    public TotalEnRateVo getCityEnRate(SysUser user, String city) {
        String finalCity = setCity(city);
        List<String> cityList = new ArrayList<>();
        if (StrUtil.isNotEmpty(user.getCity())) {
            cityList.add(user.getCity());
        }

        if (StrUtil.isNotEmpty(finalCity)) {
            cityList.add(finalCity);
        }

        List<Map<String, Object>> data = fsStoreListMapper.getCityEnRate(cityList);
        Set<String> citys = data.stream().map(item -> MapUtil.getStr(item, "city")).collect(Collectors.toSet());
        List<Integer> totalRates = new ArrayList<>();
        List<Integer> notTotalRates = new ArrayList<>();
        List<Integer> partTotalRates = new ArrayList<>();
        for (String li : citys) {
            totalRates.add(getCityEnRateNumber(data, li, RecordTypeConstant.ENTERED));
            notTotalRates.add(getCityEnRateNumber(data, li, RecordTypeConstant.NOT_ENTERED));
            partTotalRates.add(getCityEnRateNumber(data, li, RecordTypeConstant.PARTIAL_ENTRY));
        }

        TotalEnRateVo vo = new TotalEnRateVo();
        vo.setType(citys);
        vo.setTotalRates(totalRates);
        vo.setNotTotalRates(notTotalRates);
        vo.setPartRates(partTotalRates);
        return vo;
    }

    private Integer getCityEnRateNumber(List<Map<String, Object>> cityEnRate, String city, int type) {
        return cityEnRate.stream().filter(item ->
                        MapUtil.getStr(item, "city", "").equals(city)
                                &&
                                MapUtil.getInt(item, "recordType", -1).equals(type))
                .mapToInt(item -> MapUtil.getInt(item, "number", 0)).sum();
    }

    @Override
    public void export(SysUser user, int type, HttpServletResponse response) {

        List<FsStoreList> list = fsStoreListMapper.selectList(user.getCity(), ExportDataUtils.exportTypeToData(type));
        ArrayList<Map<String, Object>> courseDtoList = new ArrayList<>();
        for (FsStoreList li : list) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("城市", li.getCity());
            map.put("区域", li.getArea());
            map.put("地址", li.getAddress());
            map.put("店名", li.getName());
            map.put("店号", li.getCode());
            map.put("店长姓名", li.getManagerName());
            map.put("店铺类型", ExportDataUtils.getStoreType(li.getStoreType()));
            map.put("店铺状态", ExportDataUtils.getStoreState(li.getStoreState()));
            map.put("录入状态", ExportDataUtils.getRecordType(li.getRecordType()));
            courseDtoList.add(map);
        }
        ServletOutputStream ouPutStream = null;
        //导出成excel
        try (ExcelWriter writer = ExcelUtil.getWriter()) {
            ExportDataUtils.setWriterResponse(response, DateUtil.now());
            if (CollUtil.isNotEmpty(courseDtoList)) {
                writer.write(courseDtoList, true);
            } else {
                List<String> head = new ArrayList<>();
                head.add("城市");
                head.add("区域");
                head.add("地址");
                head.add("店名");
                head.add("店号");
                head.add("店长姓名");
                head.add("店铺类型");
                head.add("店铺状态");
                head.add("录入状态");
                writer.writeHeadRow(head);
            }
            ouPutStream = response.getOutputStream();
            writer.flush(ouPutStream, true);
        } catch (IOException e) {
            throw new RuntimeException("导出失败");
        } finally {
            IoUtil.close(ouPutStream);
        }
    }

    @Override
    public StoreDetailDingVo getDingById(String id) {
        return fsStoreListMapper.getDingById(id);
    }

    private String setCity(String city) {
        if (StrUtil.isNotEmpty(city) && city.equals("0")) {
            return null;
        }
        return city;
    }

    @Override
    public List<FsStoreList> selectList(String city, String time) {
        return fsStoreListMapper.selectList(city,time);
    }

    @Override
    public List<FsStoreList> selectAll() {
        return fsStoreListMapper.selectAll();
    }
}
