package com.uhome.file.uhomefilesystem.db.service;


import com.uhome.file.uhomefilesystem.config.utils.ResultCode;
import com.uhome.file.uhomefilesystem.db.entity.*;
import com.uhome.file.uhomefilesystem.db.param.OprateParam;
import com.uhome.file.uhomefilesystem.db.param.SysRoleParam;
import com.uhome.file.uhomefilesystem.db.param.SysUserParam;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author zbb
 * @since 2020-07-14
 */
public interface IFsSysService {

    List<FsCertificate> getDa();

    void addCer(String name, String type);

    void deleteCer(Long id);

    List<SysRole> selectRole();

    List<SysRoleMenu> selectRoleMenu(Long id);

    List<SysUser> selectUser(SysUserParam sysUserParam);

    List<SysUser> selectUserSelect(SysUserParam sysUserParam);

    List<DimEmployeeInfo> selectUser2(SysUserParam sysUserParam);

    void deleteUser(Long id);

    List<DimEmployeeInfo> selectUser2ByIds(List<String> userIds);

    void insertUser(SysUser sysUser);

    void updateRole(Long id, Long roleId);

    void saveRole(SysRoleParam sysRoleParam);

    void deleteRole(Long id);

    List<SysMenu> selectMenuList();

    String getTempId(Long id);

    List<SysMenu> selectUserMenu(Long id,Integer type);

    List<SysRole> selectRoleByName(String name);

    ResultCode saveLoginLog(FsLoginLog fsLoginLog);

    ResultCode saveOprateLog(OprateParam oprateParam);

    List<SysRole> getUserCity(Long userId);
}
