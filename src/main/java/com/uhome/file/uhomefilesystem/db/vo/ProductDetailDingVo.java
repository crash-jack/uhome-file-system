package com.uhome.file.uhomefilesystem.db.vo;

import lombok.Data;

import java.util.Date;

@Data
public class ProductDetailDingVo {

    private String ddUserId;

    private String productName;

    private String productCode;

    private String productCate1Name;

    private String productCate2Name;

    private String detailName;

    private Date remindTime;
}
