package com.uhome.file.uhomefilesystem.db.dao;

import com.uhome.file.uhomefilesystem.db.entity.DimEmployeeInfo;
import com.uhome.file.uhomefilesystem.db.entity.FsCertificate;
import com.uhome.file.uhomefilesystem.db.param.DimEmployeeParam;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DimEmployeeInfoMapper {

    List<DimEmployeeInfo> queryUserByFiled(DimEmployeeParam dimEmployeeParam);
}