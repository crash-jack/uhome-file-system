package com.uhome.file.uhomefilesystem.db.service;

import com.uhome.file.uhomefilesystem.db.param.DownloadParam;

import javax.servlet.http.HttpServletResponse;

public interface IDownloadService {

    void file(DownloadParam downloadParam, HttpServletResponse response) throws Exception;

    void batchProduct(String ids, HttpServletResponse response) throws Exception ;

    void batchStore(String ids, HttpServletResponse response) throws Exception ;


}
