package com.uhome.file.uhomefilesystem.db.service;


import com.uhome.file.uhomefilesystem.db.entity.FsStoreDetail;
import com.uhome.file.uhomefilesystem.db.vo.HStoreDetailVo;
import com.uhome.file.uhomefilesystem.db.vo.StoreDetailDingVo;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author zbb
 * @since 2020-07-14
 */
public interface IFsStoreDetailService {

    List<HStoreDetailVo> queryStoreDetailData(Long id);

    /**
     * 获取到期门店信息
     */
    List<StoreDetailDingVo> getListByRemindTime();

    int insertSelective(FsStoreDetail record);
}
