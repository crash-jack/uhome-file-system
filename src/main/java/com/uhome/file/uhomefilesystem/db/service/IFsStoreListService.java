package com.uhome.file.uhomefilesystem.db.service;


import cn.hutool.json.JSONArray;
import com.uhome.file.uhomefilesystem.db.entity.FsStoreList;
import com.uhome.file.uhomefilesystem.db.entity.SysUser;
import com.uhome.file.uhomefilesystem.db.param.FsStoreListParam;
import com.uhome.file.uhomefilesystem.db.vo.*;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author zbb
 * @since 2020-07-14
 */
public interface IFsStoreListService {

    void initStore();

    List<HStoreVo> queryStoreData(String queryStr);

    List<FsStoreList> queryStoreList(FsStoreListParam storeListParam);

    List<PStoreDetailVo> queryStoreListDetail(Long id);

    /**
     * 获取当前用户的城市和区域
     *
     * @param user user
     */
    StoreCityAreaVo getCurrUserCityAndArea(SysUser user);

    /**
     * 获取当前用户的城市和区域
     *
     * @param user user
     */
    JSONArray getCurrUserCityAndAreaForPc(SysUser user);


    /**
     * 获取门店数量信息
     */
    TotalDataVo getTotalData(String city);

    /**
     * 获取区域完成数量
     *
     * @param city 城市
     * @param area 区域
     */
    TotalBaseVo getAreaEnRate(SysUser user, String city, String area);

    /**
     * 获取城市完成情况
     */
    TotalEnRateVo getCityEnRate(SysUser user, String city);

    /**
     * 导出店铺数据
     *
     * @param type     type
     * @param response response
     */
    void export(SysUser user, int type, HttpServletResponse response);

    StoreDetailDingVo getDingById(String id);


    List<FsStoreList> selectList(@Param("city") String city,
                                 @Param("time") String time);

    List<FsStoreList> selectAll();

    void initStoreImg();

    void initGoods();


    void initGoodsImg();
}
