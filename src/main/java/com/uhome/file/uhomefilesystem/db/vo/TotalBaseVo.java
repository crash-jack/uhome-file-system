package com.uhome.file.uhomefilesystem.db.vo;

import lombok.Data;

@Data
public class TotalBaseVo {
    /**
     * 录入完成
     */
    private Integer total;

    /**
     * 未录入总数
     */
    private Integer notTotal;

    /**
     * 部分录入总数
     */
    private Integer partTotal;
}
