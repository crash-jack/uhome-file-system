package com.uhome.file.uhomefilesystem.db.service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author bowen.li
 * @since 2021-08-24
 */
public interface IFsProductQueryService {

    /**
     * 查询查询次数
     */
    List<Map<String, Object>> getCountTotal(Integer type, String name);

    void export(Integer type, HttpServletResponse response);

    void productLog(Integer type, Long productId, Long certificateId);
}
