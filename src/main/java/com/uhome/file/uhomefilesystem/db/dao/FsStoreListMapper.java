package com.uhome.file.uhomefilesystem.db.dao;

import com.uhome.file.uhomefilesystem.db.entity.FsStoreList;
import com.uhome.file.uhomefilesystem.db.param.FsStoreListParam;
import com.uhome.file.uhomefilesystem.db.vo.HStoreVo;
import com.uhome.file.uhomefilesystem.db.vo.PStoreDetailVo;
import com.uhome.file.uhomefilesystem.db.vo.StoreDetailDingVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface FsStoreListMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table fs_store_list
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table fs_store_list
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    int insert(FsStoreList record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table fs_store_list
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    int insertSelective(FsStoreList record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table fs_store_list
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    FsStoreList selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table fs_store_list
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    int updateByPrimaryKeySelective(FsStoreList record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table fs_store_list
     *
     * @mbg.generated Sun Jul 04 16:57:51 CST 2021
     */
    int updateByPrimaryKey(FsStoreList record);

    List<HStoreVo> queryStoreData(String queryStr);

    List<FsStoreList> selectAll();

    List<FsStoreList> selectList(@Param("city") String city,
                                 @Param("time") String time);

    List<FsStoreList> selectListGroup(@Param("cityList") String[] cityList,
                                      @Param("time") String time);

    int selectCount(@Param("recordType") Integer recordType,
                    @Param("city") String city,
                    @Param("userCity") String userCity,
                    @Param("area") String area);

    List<FsStoreList> queryStoreList(FsStoreListParam storeListParam);

    List<PStoreDetailVo> queryStoreListDetail(Long id);

    List<Map<String, Object>> getCityEnRate(@Param("cityList") List<String> cityList);

    StoreDetailDingVo getDingById(String id);
}